﻿let dataTable;

$(document).ready(function () {
    var url = window.location.search;
    if (url.includes("inprocess")) return loadDataTable("inprocess");
    if (url.includes("completed")) return loadDataTable("completed");
    if (url.includes("pending")) return loadDataTable("pending");
    if (url.includes("approved")) return loadDataTable("approved");

    return loadDataTable("all");
});

function loadDataTable(status) {
    dataTable = $('#tblTable').DataTable({
        "ajax": { url: '/admin/order/getall?status=' + status },
        "columns": [
            { data: "Id", width: "5%" },
            { data: "Name", width: "15%" },
            { data: "PhoneNumber", width: "20%" },
            { data: "ApplicationUser.Email", width: "15%" },
            { data: "OrderStatus", width: "10%" },
            { data: "OrderTotal", width: "10%" },
            {
                data: "Id",
                "render": function (data) {
                    return `<div class="w-75 btn-group text-end" role="group">
                                <a href="/admin/order/details?orderId=${data}" class="btn btn-primary w-100 mx-1">
                                    <i class="bi bi-pencil-square"></i>
                                </a>
                            </div>`
                },
                "width": "25%"
            }
        ]
    });
}