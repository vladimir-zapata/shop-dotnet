﻿let dataTable;

$(document).ready(function () {
    loadDataTable();
});

function loadDataTable() {
    dataTable = $('#tblTable').DataTable({
        "ajax": { url: '/admin/category/getall' },
        "columns": [
            { data: "Name", width: "25%" },
            { data: "DisplayOrder", width: "25%" },
            {
                data: "Id",
                "render": function (data) {
                    return `<div class="w-75 btn-group text-end" role="group">
                                <a href="/admin/category/upsert?id=${data}" class="btn btn-primary w-100 mx-1">
                                    <i class="bi bi-pencil-square"></i>
                                    EDIT
                                </a>
                                <a onClick=Delete("/admin/category/delete?id=${data}") class="btn btn-danger w-100 mx-1">
                                    <i class="bi bi-trash-fill"></i>
                                    DELETE
                                </a>
                            </div>`
                },
                "width": "50%"
            }
        ]
    });
}

function Delete(url) {
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: url,
                type: "DELETE",
                success: function (data) {
                    dataTable.ajax.reload();
                    toastr.success(data.message);
                }
            })
        }
    })
};