﻿using Shop_Dotnet.BLL.Models;
using Shop_Dotnet.DAL.Entities;

namespace Shop_Dotnet.Web.Areas.Admin.ViewModels
{
	public class OrderVM
	{
		public OrderHeaderModel? OrderHeader { get; set; }
		public IEnumerable<OrderDetail>? OrderDetail { get; set; }
	}
}
