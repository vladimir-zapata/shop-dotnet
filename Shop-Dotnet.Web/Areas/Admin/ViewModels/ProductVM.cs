﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Shop_Dotnet.BLL.Models;
using Shop_Dotnet.DAL.Entities;

namespace Shop_Dotnet.Web.Areas.Admin.ViewModels
{
    public class ProductVM
    {
        public string? CategoryName { get; set; }
        public Product? Product { get; set; }
        public IEnumerable<SelectListItem>? Categories { get; set; }
        public List<ProductModel>? Products { get; set; }
    }
}
