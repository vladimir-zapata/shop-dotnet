﻿using Shop_Dotnet.BLL.Models;

namespace Shop_Dotnet.Web.Areas.Admin.ViewModels
{
    public class CompanyVM
    {
        public CompanyModel Company { get; set; } = null!;
        public List<CompanyModel>? CompanyList { get; set; }
    }
}
