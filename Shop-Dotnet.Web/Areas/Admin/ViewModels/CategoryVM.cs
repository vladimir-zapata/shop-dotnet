﻿using Shop_Dotnet.BLL.Models;

namespace Shop_Dotnet.Web.Areas.Admin.ViewModels
{
    public class CategoryVM
    {
        public CategoryModel Category { get; set; } = null!;
        public List<CategoryModel>? CategoryList { get; set; }
    }
}
