﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NuGet.Protocol;
using Shop_Dotnet.BLL.Contract;
using Shop_Dotnet.BLL.Extensions;
using Shop_Dotnet.BLL.Models;
using Shop_Dotnet.BLL.Services;
using Shop_Dotnet.DAL.Entities;
using Shop_Dotnet.Utility;
using Shop_Dotnet.Web.Areas.Admin.ViewModels;
using Shop_Dotnet.Web.Areas.Customer.ViewModels;
using Stripe;
using Stripe.Checkout;
using System.Security.Claims;

namespace Shop_Dotnet.Web.Areas.Admin.Controllers
{
	[Area("Admin")]
    [Authorize]
    public class OrderController : Controller
	{
		private readonly IOrderHeaderService _orderHeaderService;
		private readonly IOrderDetailService _orderDetailService;

		[BindProperty]
		public OrderVM OrderVM { get; set; } = new();

		public OrderController(IOrderHeaderService orderHeaderService, IOrderDetailService orderDetailService)
		{
			_orderHeaderService = orderHeaderService;
			_orderDetailService = orderDetailService;
		}

		public IActionResult Index()
		{ 
			return View();
		}

        public async Task<IActionResult> Details(int orderId)
        {
			var orderResult = await _orderHeaderService.GetById(orderId);
			var order = (OrderHeaderModel)orderResult.Data!;

			var orderDetails = await _orderDetailService.GetAll();
			var orderDetailsList = (IEnumerable<OrderDetail>)orderDetails.Data!;

			OrderVM orderVM = new()
			{
				OrderHeader = order,
				OrderDetail = orderDetailsList.Where(x => x.OrderHeaderId == orderId).ToList()
            };

            return View(orderVM);
        }

		[HttpPost]
		[Authorize(Roles = StaticDetails.ROLE_ADMIN + "," + StaticDetails.ROLE_EMPLOYEE)]
        public async Task<IActionResult> UpdateOrdelDetail()
        {
            var orderResult = await _orderHeaderService.GetById(OrderVM.OrderHeader!.Id);
            var orderHeader = (OrderHeaderModel)orderResult.Data!;

			orderHeader.Name = OrderVM.OrderHeader.Name;
			orderHeader.PhoneNumber = OrderVM.OrderHeader.PhoneNumber;
			orderHeader.StreetAddress = OrderVM.OrderHeader.StreetAddress;
			orderHeader.City = OrderVM.OrderHeader.City;
			orderHeader.State = OrderVM.OrderHeader.State;
			orderHeader.PostalCode = OrderVM.OrderHeader.PostalCode;

			if (!string.IsNullOrEmpty(OrderVM.OrderHeader.Carrier)) 
			{
				orderHeader.Carrier = OrderVM.OrderHeader.Carrier;
			}

            if (!string.IsNullOrEmpty(OrderVM.OrderHeader.TrackingNumber))
            {
                orderHeader.Carrier = OrderVM.OrderHeader.TrackingNumber;
            }

            var orderHeaderToUpdate = await _orderHeaderService.UpdateOrderHeader(orderHeader);

			TempData["success"] = "Order details updated sucessfully.";

            return RedirectToAction(nameof(Details), new { orderId = orderHeader.Id });
        }

        [HttpPost]
        [Authorize(Roles = StaticDetails.ROLE_ADMIN + "," + StaticDetails.ROLE_EMPLOYEE)]
        public async Task<IActionResult> StartProcessing()
        {
			var orderHeaderResult = await _orderHeaderService.UpdateStatus(OrderVM.OrderHeader!.Id, StaticDetails.StatusInProcess);

			if (!orderHeaderResult.Success)
			{
				TempData["error"] = "Order status failed to update.";
			}
			else 
			{
                TempData["success"] = "Order status changed to processing";
            }

            return RedirectToAction(nameof(Details), new { orderId = OrderVM.OrderHeader.Id });
        }

        [HttpPost]
        [Authorize(Roles = StaticDetails.ROLE_ADMIN + "," + StaticDetails.ROLE_EMPLOYEE)]
        public async Task<IActionResult> ShipOrder()
        {
			var orderHeader = await _orderHeaderService.ShipOrder(OrderVM.OrderHeader!);

			if (!orderHeader.Success) 
			{
                TempData["error"] = "Order status failed to update.";
                return RedirectToAction(nameof(Details), new { orderId = OrderVM.OrderHeader!.Id });
            }

            var orderHeaderResult = await _orderHeaderService.UpdateStatus(OrderVM.OrderHeader!.Id, StaticDetails.StatusShipped);

            if (!orderHeaderResult.Success)
            {
                TempData["error"] = "Order status failed to update.";
            }
            else
            {
                TempData["success"] = "Order set to shipment.";
            }

            return RedirectToAction(nameof(Details), new { orderId = OrderVM.OrderHeader.Id });
        }

        [HttpPost]
        [Authorize(Roles = StaticDetails.ROLE_ADMIN + "," + StaticDetails.ROLE_EMPLOYEE)]
        public async Task<IActionResult> CancelOrder()
        {
            var orderHeader = await _orderHeaderService.GetById(OrderVM.OrderHeader!.Id);

            if (orderHeader == null || !orderHeader.Success)
            {
                TempData["error"] = "Failed to cancel order";

                return RedirectToAction(nameof(Details), new { orderId = OrderVM.OrderHeader.Id });
            }

			if (orderHeader.Data!.PaymentStatus == StaticDetails.PaymentStatusApproved)
			{
				var options = new RefundCreateOptions
				{
					Reason = RefundReasons.RequestedByCustomer,
					PaymentIntent = orderHeader.Data.PaymentIntentId,
				};

				var service = new RefundService();
				Refund refund = service.Create(options);

				await _orderHeaderService.UpdateStatus(OrderVM.OrderHeader.Id, StaticDetails.StatusCancelled, StaticDetails.StatusRefunded);
			}
			else 
			{
                await _orderHeaderService.UpdateStatus(OrderVM.OrderHeader.Id, StaticDetails.StatusCancelled, StaticDetails.StatusCancelled);
            }

            TempData["success"] = "Order cancelled succesfully";
            return RedirectToAction(nameof(Details), new { orderId = OrderVM.OrderHeader.Id });
        }

		[HttpPost]
		[ActionName("Details")]
		public async Task<IActionResult> Details_PAY_NOW() 
		{
            var orderHeader = await _orderHeaderService.GetById(OrderVM.OrderHeader!.Id);
            var orderDetail = await _orderDetailService.GetAll();
            List<OrderDetail> orderDetails = orderDetail.Data!;

            OrderVM.OrderHeader = orderHeader.Data!;
            OrderVM.OrderDetail = orderDetails.Where(x => x.OrderHeaderId == OrderVM.OrderHeader.Id).ToList();

            var domain = Request.Scheme + "://" + Request.Host.Value + "/";

            var options = new SessionCreateOptions
            {
                SuccessUrl = domain + $"admin/order/PaymentConfirmation?orderHeaderId={OrderVM.OrderHeader.Id}",
                CancelUrl = domain + $"admin/order/details?orderId={OrderVM.OrderHeader.Id}",
                LineItems = new List<SessionLineItemOptions>(),
                Mode = "payment",
            };

            foreach (var item in OrderVM.OrderDetail)
            {
                var sessionLineItem = new SessionLineItemOptions()
                {
                    PriceData = new SessionLineItemPriceDataOptions
                    {
                        UnitAmount = (long)(item.Price * 100), // $20.50 => 2050
                        Currency = "usd",
                        ProductData = new SessionLineItemPriceDataProductDataOptions
                        {
                            Name = item.Product!.Title
                        }
                    },
                    Quantity = item.Count
                };

                options.LineItems.Add(sessionLineItem);
            }

            var service = new SessionService();
            Session session = service.Create(options);

            var r1 = await _orderHeaderService
                .UpdateStripePaymentId(OrderVM.OrderHeader.Id, session.Id, session.PaymentIntentId);
            Response.Headers.Add("Location", session.Url);
            return new StatusCodeResult(303);
        }

        public async Task<IActionResult> PaymentConfirmation(int orderHeaderId)
        {
            var claimsIdentity = (ClaimsIdentity)User.Identity!;
            var userId = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier)!.Value;

            var orderHeader = await _orderHeaderService.GetById(orderHeaderId);

            if (orderHeader == null || orderHeader.Data == null) return View(orderHeaderId);

            if (orderHeader!.Data!.PaymentStatus == StaticDetails.PaymentStatusDelayedPayment)
            {
                var service = new SessionService();
                Session session = service.Get(orderHeader.Data.SessionId);

                if (session.PaymentStatus.ToLower() == "paid")
                {
                    await _orderHeaderService.UpdateStripePaymentId(orderHeaderId, session.Id, session.PaymentIntentId);
                    await _orderHeaderService.UpdateStatus(orderHeaderId, orderHeader.Data!.OrderStatus, StaticDetails.PaymentStatusApproved);
                }
            }

            return View(orderHeaderId);
        }

        #region API Calls
        [HttpGet]
		public async Task<IActionResult> GetAll(string status)
		{
            var claimsIdentity = (ClaimsIdentity)User.Identity!;
            var userId = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier)!.Value;

            List<OrderHeaderModel> orders;

			if (User.IsInRole(StaticDetails.ROLE_ADMIN) || User.IsInRole(StaticDetails.ROLE_EMPLOYEE))
			{
				var orderHeadersResult = await _orderHeaderService.GetAll();
				orders = orderHeadersResult.Data!;
			}
			else
			{
				var orderHeadersResult = await _orderHeaderService.GetAllByApplicationUserId(userId);
				orders = orderHeadersResult.Data!;
            }

            switch (status)
			{
				case "pending":
					orders = orders.Where(x => x.PaymentStatus == StaticDetails.PaymentStatusDelayedPayment).ToList();
					break;
				case "inprocess":
					orders = orders.Where(x => x.OrderStatus == StaticDetails.StatusInProcess).ToList();
					break;
				case "completed":
					orders = orders.Where(x => x.OrderStatus == StaticDetails.StatusShipped).ToList();
					break;
				case "approved":
					orders = orders.Where(x => x.OrderStatus == StaticDetails.StatusApproved).ToList();
					break;
				default:
					break;
			}

            string responseJson = $"{{ \"data\": {orders.ToJson()} }}";


            if (orders != null)
			{
				return Content(responseJson, "application/json");
			}
			else
			{
				return Problem();
			}
		}
		#endregion
	}
}
