﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NuGet.Protocol;
using Shop_Dotnet.BLL.Contract;
using Shop_Dotnet.BLL.Core;
using Shop_Dotnet.BLL.Dto.Company;
using Shop_Dotnet.BLL.Extensions;
using Shop_Dotnet.BLL.Models;
using Shop_Dotnet.DAL.Entities;
using Shop_Dotnet.Utility;
using Shop_Dotnet.Web.Areas.Admin.ViewModels;
using System.Data;
using System.Security.Claims;

namespace Shop_Dotnet.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = StaticDetails.ROLE_ADMIN)]
    public class CompanyController : Controller
    {
        private readonly ICompanyService _companyService;

        public CompanyController(ICompanyService companyService)
        {
            _companyService = companyService;
        }

        public async Task<IActionResult> Index()
        {
            CompanyVM companyVM = new();
            var companies = await _companyService.GetAll();

            if (companies?.Data != null)
            {
                companyVM.CompanyList = companies.Data;
            }

            return View(companyVM);
        }

        public async Task<IActionResult> Upsert(int? id)
        {
            CompanyVM companyVM = new();

            if (id == null || id == 0)
            {
                return View(companyVM);
            }
            else
            {
                var company = await _companyService.GetById(id!.Value);

                if (company == null) return NotFound();

                companyVM.Company = new CompanyModel();
                companyVM.Company = company.Data!;
                

                return View(companyVM);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Upsert(CompanyVM companyVM)
        {
            if (ModelState.IsValid)
            {
                ServiceResult serviceResult = new();

                var claimsIdentity = (ClaimsIdentity)User.Identity!;
                var userId = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier)!.Value;

                //Create
                if (companyVM.Company.Id == null || companyVM.Company.Id == 0)
                {
                    var saveCompanyDto = companyVM.Company.GetSaveCompanyDtoFromCompanyModel();
                    saveCompanyDto.RequestUser = userId;

                    serviceResult = await _companyService.SaveCompany(saveCompanyDto);
                }
                //Update
                else
                {
                    var updateCompanyDto = companyVM.Company.GetUpdateCompanyDtoFromCompanyModel();
                    updateCompanyDto.RequestUser = userId;

                    serviceResult = await _companyService.UpdateCompany(updateCompanyDto);
                }

                if (serviceResult == null || !serviceResult.Success)
                {
                    TempData["error"] = "Failed to create company.";
                    return View(companyVM);
                }

                TempData["success"] = serviceResult.Message;

                return RedirectToAction("Index");
            }
            else
            {
                return View(companyVM);
            }
        }

        public async Task<IActionResult> Delete(int? id)
        {
            var claimsIdentity = (ClaimsIdentity)User.Identity!;
            var userId = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier)!.Value;

            var deleteCompanyDto = new DeleteCompanyDto
            {
                Id = id,
                RequestUser = userId
            };

            ServiceResult serviceResult = await _companyService.DeleteCompany(deleteCompanyDto);

            if (serviceResult == null || !serviceResult.Success)
            {
                return Json(new { success = false, message = "Error while deleting" });
            }

            if (serviceResult == null || !serviceResult.Success)
            {
                return Json(new { success = false, message = "Error while deleting" });
            }

            return Json(new { success = true, message = "Deleted successfully" });
        }

        #region API Calls
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var companies = await _companyService.GetAll();

            JObject json = JObject.Parse(companies.ToJson());
            JToken data = json["Data"]!;

            string jsonResult = data.ToString(Formatting.None);

            string responseJson = $"{{ \"data\": {jsonResult} }}";

            if (companies?.Data != null)
            {
                return Content(responseJson, "application/json");
            }
            else
            {
                return Problem();
            }
        }
        #endregion
    }
}
