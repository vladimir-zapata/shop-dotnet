﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NuGet.Protocol;
using Shop_Dotnet.BLL.Contract;
using Shop_Dotnet.BLL.Core;
using Shop_Dotnet.BLL.Dto.Product;
using Shop_Dotnet.BLL.Extensions;
using Shop_Dotnet.DAL.Entities;
using Shop_Dotnet.Utility;
using Shop_Dotnet.Web.Areas.Admin.ViewModels;
using System.Security.Claims;

namespace Shop_Dotnet.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = StaticDetails.ROLE_ADMIN)]
    public class ProductController : Controller
    {
        private readonly IProductService _productService;
        private readonly ICategoryService _categoryService;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public ProductController(IProductService productService, ICategoryService categoryService, IWebHostEnvironment webHostEnvironment)
        {
            this._productService = productService;
            this._categoryService = categoryService;
            this._webHostEnvironment = webHostEnvironment;
        }

        public async Task<IActionResult> Index()
        {
            ProductVM productVM = new();

            var products = await _productService.GetAll();

            if (products?.Data != null)
            {
                productVM.Products = products.Data;
            }

            return View(productVM);
        }

        public async Task<IActionResult> Upsert(int? id)
        {
            ProductVM productVM = new();
            var categories = await _categoryService.GetAll();

            List<SelectListItem> categoryList = new List<SelectListItem>();

            foreach (var category in categories.Data!)
            {
                SelectListItem listItem = new SelectListItem
                {
                    Value = category.Id != null ? category.Id.ToString() : string.Empty,
                    Text = category.Name != null ? category.Name : string.Empty
                };
                categoryList.Add(listItem);
            }

            //Create
            if (id == 0 || id == null)
            {
                if (categories?.Data != null)
                {
                    productVM.Categories = categoryList;
                }
            }
            //Update
            else
            {
                var product = await _productService.GetById(id!.Value);

                if (product == null || categories == null) return NotFound();

                if (product.Data == null || categories?.Data == null) return RedirectToAction("Index");

                productVM.Product = new Product() 
                {
                    Id = product.Data!.Id,
                    Title = product.Data!.Title,
                    Author = product.Data!.Author,
                    Description = product.Data!.Description,
                    ISBN = product.Data!.ISBN,
                    ListPrice = product.Data!.ListPrice,
                    Price = product.Data!.Price,
                    Price50 = product.Data!.Price50,
                    Price100 = product.Data!.Price100,
                    ImageUrl = product.Data!.ImageUrl,
                    CategoryId = product.Data!.CategoryId,
                };

                productVM.Categories = categoryList;
            }

            return View(productVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Upsert(ProductVM productVM, IFormFile? file)
        {
            if (ModelState.IsValid)
            {
                string wwwRootPath = _webHostEnvironment.WebRootPath;

                if (file != null)
                {
                    string fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
                    string productPath = Path.Combine(wwwRootPath, @"images\product");

                    if (!string.IsNullOrEmpty(productVM.Product?.ImageUrl))
                    {
                        var oldImagePath =
                            Path.Combine(wwwRootPath, productVM.Product.ImageUrl.TrimStart('\\'));

                        if (System.IO.File.Exists(oldImagePath))
                        {
                            System.IO.File.Delete(oldImagePath);
                        }
                    }

                    using var fileStream = new FileStream(Path.Combine(productPath, fileName), FileMode.Create);
                    file.CopyTo(fileStream);

                    productVM.Product!.ImageUrl = @"\images\product\" + fileName;
                }

                ServiceResult serviceResult = new();

                var claimsIdentity = (ClaimsIdentity)User.Identity!;
                var userId = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier)!.Value;

                //Create
                if (productVM.Product?.Id == null || productVM.Product.Id == 0)
                {
                    var saveProductDto = productVM.Product!.GetSaveProductDtoFromProduct();
                    saveProductDto.RequestUser = userId;

                    serviceResult = await _productService.SaveProduct(saveProductDto);
                }
                //Update
                else
                {
                    var updateProductDto = productVM.Product?.GetUpdateProductDtoFromProduct();
                    updateProductDto!.RequestUser = userId;

                    serviceResult = await _productService.UpdateProduct(updateProductDto);
                }

                if (serviceResult == null || !serviceResult.Success)
                {
                    TempData["error"] = "Failed to create product.";
                    return RedirectToAction("Index");
                }

                TempData["success"] = serviceResult.Message;
                return RedirectToAction("Index");
            }
            else
            {
                var categories = await _categoryService.GetAll();

                List<SelectListItem> categoryList = new List<SelectListItem>();

                foreach (var category in categories.Data!)
                {
                    SelectListItem listItem = new SelectListItem
                    {
                        Value = category.Id != null ? category.Id.ToString() : string.Empty,
                        Text = category.Name != null ? category.Name : string.Empty
                    };
                    categoryList.Add(listItem);
                }

                productVM.Categories = categoryList;

                return View(productVM);
            }
        }
        [HttpDelete]
        public async Task<IActionResult> Delete(int? id)
        {
            var product = await _productService.GetById(id!.Value);

            if (product == null || product.Data == null)
            {
                return Json(new { success = false, message = "Error while deleting" });
            }

            string imageUrl = product!.Data!.ImageUrl;

            var oldImagePath =
                    Path.Combine(_webHostEnvironment.WebRootPath, imageUrl.TrimStart('\\'));

            if (System.IO.File.Exists(oldImagePath))
            {
                System.IO.File.Delete(oldImagePath);
            }

            var claimsIdentity = (ClaimsIdentity)User.Identity!;
            var userId = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier)!.Value;

            var deleteProductDto = new DeleteProductDto() 
            {
                Id = id,
                RequestUser = userId
            };

            var deleteResponse = await _productService.DeleteProduct(deleteProductDto);

            if (deleteResponse == null || !deleteResponse.Success)
            {
                return Json(new { success = false, message = "Error while deleting" });
            }

            return Json(new { success = true, message = "Deleted successfully" });
        }

        #region API Calls
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var products = await _productService.GetAll();

            JObject json = JObject.Parse(products.ToJson());
            JToken data = json["Data"]!;

            string jsonResult = data.ToString(Formatting.None);

            string responseJson = $"{{ \"data\": {jsonResult} }}";

            if (products?.Data != null)
            {
                return Content(responseJson, "application/json");
            }
            else
            {
                return Problem();
            }
        }
        #endregion
    }
}