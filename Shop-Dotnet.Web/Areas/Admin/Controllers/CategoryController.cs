﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http.Json;
using Shop_Dotnet.BLL.Contract;
using Shop_Dotnet.BLL.Core;
using Shop_Dotnet.BLL.Dto.Category;
using Shop_Dotnet.DAL.Entities;
using Shop_Dotnet.Utility;
using Shop_Dotnet.Web.Areas.Admin.ViewModels;
using System.Security.Claims;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NuGet.Protocol;
using Shop_Dotnet.BLL.Models;

namespace Shop_Dotnet.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = StaticDetails.ROLE_ADMIN)]
    public class CategoryController : Controller
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        public async Task<IActionResult> Index()
        {
            CategoryVM categoryVM = new();
            var categories = await _categoryService.GetAll();

            if (categories?.Data != null)
            {
                categoryVM.CategoryList = categories.Data;
            }

            return View(categoryVM);
        }
        public async Task<IActionResult> Upsert(int? id)
        {
            CategoryVM categoryVM = new();

            if (id == null || id == 0)
            {
                return View(categoryVM);
            }
            else
            {
                var category = await _categoryService.GetById(id.Value);

                if (category == null) return NotFound();

                categoryVM.Category = new CategoryModel();
                categoryVM.Category = category.Data!;

                return View(categoryVM);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Upsert(CategoryVM categoryVM)
        {
            if (categoryVM.Category.Name == categoryVM.Category.DisplayOrder.ToString())
            {
                ModelState.AddModelError("name", "Display Order cannot exactly match Category Name.");
            }

            if (ModelState.IsValid)
            {
                var claimsIdentity = (ClaimsIdentity)User.Identity!;
                var userId = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier)!.Value;

                ServiceResult serviceResult = new();

                //Create
                if (categoryVM.Category.Id == null || categoryVM.Category.Id == 0)
                {
                    var saveCategoryDto = new SaveCategoryDto
                    {
                        Name = categoryVM.Category.Name,
                        DisplayOrder = categoryVM.Category.DisplayOrder,
                        RequestUser = userId
                    };

                    serviceResult = await _categoryService.SaveCategory(saveCategoryDto);
                }
                else
                {
                    var updateCategoryDto = new UpdateCategoryDto
                    {
                        Id =categoryVM.Category.Id,
                        Name = categoryVM.Category.Name,
                        DisplayOrder = categoryVM.Category.DisplayOrder,
                        RequestUser = userId
                    };

                    serviceResult = await _categoryService.UpdateCategory(updateCategoryDto);
                }

                if (serviceResult == null || !serviceResult.Success)
                {
                    TempData["error"] = "Failed to create category.";
                    return View(categoryVM);
                }

                TempData["success"] = serviceResult.Message;

                return RedirectToAction("Index");
            }
            else
            {
                return View(categoryVM);
            }
        }

        public async Task<IActionResult> Delete(int? id)
        {
            var claimsIdentity = (ClaimsIdentity)User.Identity!;
            var userId = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier)!.Value;

            var deleteCategoryDto = new DeleteCategoryDto
            {
                Id = id,
                RequestUser = userId
            };

            ServiceResult serviceResult = await _categoryService.DeleteCategory(deleteCategoryDto);

            if (serviceResult == null || !serviceResult.Success)
            {
                return Json(new { success = false, message = "Error while deleting" });
            }

            if (serviceResult == null || !serviceResult.Success)
            {
                return Json(new { success = false, message = "Error while deleting" });
            }

            return Json(new { success = true, message = "Deleted successfully" });
        }

        #region API Calls
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var categories = await _categoryService.GetAll();

            JObject json = JObject.Parse(categories.ToJson());
            JToken data = json["Data"]!;

            string jsonResult = data.ToString(Formatting.None);

            string responseJson = $"{{ \"data\": {jsonResult} }}";

            if (categories?.Data != null)
            {
                return Content(responseJson, "application/json");
            }
            else
            {
                return Problem();
            }
        }
        #endregion
    }
}
