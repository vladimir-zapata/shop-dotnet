﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shop_Dotnet.BLL.Contract;
using Shop_Dotnet.BLL.Extensions;
using Shop_Dotnet.BLL.Models;
using Shop_Dotnet.DAL.Entities;
using Shop_Dotnet.Utility;
using Shop_Dotnet.Web.Areas.Customer.ViewModels;
using Stripe.Checkout;
using System.Security.Claims;
using static System.Net.WebRequestMethods;

namespace Shop_Dotnet.Web.Areas.Customer.Controllers
{ 
    [Area("Customer")]
    [Authorize]
    public class CartController : Controller
    {
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IApplicationUserService _applicationUserService;
        private readonly IOrderHeaderService _orderHeaderService;
        private readonly IOrderDetailService _orderDetailService;

        [BindProperty]
        public ShoppingCartVM? ShoppingCartVM { get; set; }

        public CartController(
            IShoppingCartService shoppingCartService, 
            IApplicationUserService applicationUserService, 
            IOrderHeaderService orderHeaderService,
            IOrderDetailService orderDetailService
            )
        {
            _shoppingCartService = shoppingCartService;
            _applicationUserService = applicationUserService;
            _orderHeaderService = orderHeaderService;
            _orderDetailService = orderDetailService;
        }

        public async Task<IActionResult> Index()
        {
            var claimsIdentity = (ClaimsIdentity)User.Identity!;
            var userId = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier)!.Value;

            var claim = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier);

            if (claim != null)
            {
                List<ShoppingCartModel> shoppingCarts = _shoppingCartService.GetAllByUserId(claim.Value).GetAwaiter().GetResult().Data!;
                HttpContext.Session.SetInt32(StaticDetails.SessionCart, shoppingCarts.Count());
            }

            var serviceResult = await _shoppingCartService.GetAllByUserId(userId);

            ShoppingCartVM = new()
            {
                ShoppingCartList = serviceResult.Data,
                OrderHeader = new()
            };

            foreach (var cart in ShoppingCartVM.ShoppingCartList!) 
            {
                cart.Price = GetPriceBaseOnQuantity(cart);
                ShoppingCartVM.OrderHeader.OrderTotal += (cart.Price * cart.Count);
            }

            return View(ShoppingCartVM);
        }
        public async Task<IActionResult> Summary()
        {
            var claimsIdentity = (ClaimsIdentity)User.Identity!;
            var userId = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier)!.Value;

            var shoppingCartResult = await _shoppingCartService.GetAllByUserId(userId);
            var userResult = await _applicationUserService.GetById(userId);

            ShoppingCartVM = new()
            {
                ShoppingCartList = shoppingCartResult.Data,
                OrderHeader = new()
            };

            ShoppingCartVM.OrderHeader.ApplicationUser = userResult.Data!;
            ShoppingCartVM.OrderHeader.Name = ShoppingCartVM.OrderHeader.ApplicationUser.Name!;
            ShoppingCartVM.OrderHeader.PhoneNumber = ShoppingCartVM.OrderHeader.ApplicationUser.PhoneNumber!;
            ShoppingCartVM.OrderHeader.StreetAddress = ShoppingCartVM.OrderHeader.ApplicationUser.StreetAddress!;
            ShoppingCartVM.OrderHeader.City = ShoppingCartVM.OrderHeader.ApplicationUser.City!;
            ShoppingCartVM.OrderHeader.State = ShoppingCartVM.OrderHeader.ApplicationUser.State!;
            ShoppingCartVM.OrderHeader.PostalCode = ShoppingCartVM.OrderHeader.ApplicationUser.PostalCode!;

            foreach (var cart in ShoppingCartVM.ShoppingCartList!)
            {
                cart.Price = GetPriceBaseOnQuantity(cart);
                ShoppingCartVM.OrderHeader.OrderTotal += (cart.Price * cart.Count);
            }

            return View(ShoppingCartVM);
        }

        [HttpPost]
        [ActionName("Summary")]
		public async Task<IActionResult> SummaryPOST()
		{
			var claimsIdentity = (ClaimsIdentity)User.Identity!;
			var userId = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier)!.Value;

			var shoppingCartResult = await _shoppingCartService.GetAllByUserId(userId);
			var userResult = await _applicationUserService.GetById(userId);

            ShoppingCartVM!.ShoppingCartList = shoppingCartResult.Data;
            ShoppingCartVM!.OrderHeader.ApplicationUserId = userId;
            ShoppingCartVM.OrderHeader.OrderDate = DateTime.Now;

            ApplicationUser applicationUser = userResult.Data!;

			foreach (var cart in ShoppingCartVM.ShoppingCartList!)
			{
				cart.Price = GetPriceBaseOnQuantity(cart);
				ShoppingCartVM.OrderHeader.OrderTotal += (cart.Price * cart.Count);
			}

            if (applicationUser.CompanyId.GetValueOrDefault() == 0)
            {
                ShoppingCartVM.OrderHeader.PaymentStatus = StaticDetails.PaymentStatusPending;
                ShoppingCartVM.OrderHeader.OrderStatus = StaticDetails.StatusPending;
            }
            else
            {
                ShoppingCartVM.OrderHeader.PaymentStatus = StaticDetails.PaymentStatusDelayedPayment;
                ShoppingCartVM.OrderHeader.OrderStatus = StaticDetails.StatusApproved;
            }

            await _orderHeaderService.SaveOrderHeader(ShoppingCartVM.OrderHeader);


            foreach (var cart in ShoppingCartVM.ShoppingCartList) 
            {
                OrderDetail orderDetail = new OrderDetail() 
                {
                    ProductId = cart.ProductId!.Value,
                    OrderHeaderId = ShoppingCartVM.OrderHeader.Id!.Value,
                    Price = cart.Price,
                    Count = cart.Count,
                };

                await _orderDetailService.SaveOrderDetail(orderDetail);
            }

			if (applicationUser.CompanyId.GetValueOrDefault() == 0)
            {
                //Stripe logic
                var domain = Request.Scheme + "://" + Request.Host.Value + "/";

				var options = new SessionCreateOptions
				{
					SuccessUrl = domain + $"customer/cart/OrderConfirmation?id={ShoppingCartVM.OrderHeader.Id}",
                    CancelUrl= domain + "customer/cart/index",
					LineItems = new List<SessionLineItemOptions>(),
					Mode = "payment",
				};

                foreach (var item in ShoppingCartVM.ShoppingCartList) 
                {
                    var sessionLineItem = new SessionLineItemOptions()
                    {
                        PriceData = new SessionLineItemPriceDataOptions
                        {
                            UnitAmount = (long)(item.Price * 100), // $20.50 => 2050
                            Currency = "usd",
                            ProductData = new SessionLineItemPriceDataProductDataOptions
                            {
                                Name = item.Product!.Title
                            }
                        },
                        Quantity = item.Count
                    };

                    options.LineItems.Add(sessionLineItem);
                }

				var service = new SessionService();
				Session session = service.Create(options);

                var r1 = await _orderHeaderService
                    .UpdateStripePaymentId(ShoppingCartVM.OrderHeader.Id!.Value, session.Id, session.PaymentIntentId);
                Response.Headers.Add("Location", session.Url);
                return new StatusCodeResult(303);
			}

			return RedirectToAction(nameof(OrderConfirmation), new { id = ShoppingCartVM.OrderHeader.Id });
		}

        public async Task<IActionResult> OrderConfirmation(int id) 
        {
			var claimsIdentity = (ClaimsIdentity)User.Identity!;
			var userId = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier)!.Value;

			var orderHeader = await _orderHeaderService.GetById(id);

            if (orderHeader == null || orderHeader.Data == null) return View(id);

			if (orderHeader!.Data!.PaymentStatus != StaticDetails.PaymentStatusDelayedPayment)
			{
                var service = new SessionService();
                Session session = service.Get(orderHeader.Data.SessionId);

                if (session.PaymentStatus.ToLower() == "paid") 
                {
                    await _orderHeaderService.UpdateStripePaymentId(id, session.Id, session.PaymentIntentId);
                    await _orderHeaderService.UpdateStatus(id, StaticDetails.StatusApproved, StaticDetails.PaymentStatusApproved);
                }
			}

            await _shoppingCartService.DeleteAllShoppingCart(userId);

            return View(id);
        }

		public async Task<IActionResult> Plus(int cartId) 
        {
            ShoppingCartModel cartFromDB = _shoppingCartService.GetById(cartId).GetAwaiter().GetResult().Data!;

            if (cartFromDB != null) 
            {
                cartFromDB.Count += 1;
                await _shoppingCartService.UpdateShoppingCart(cartFromDB.GetUpdateShoppingCartDtoFromShoppingCartModel());
            }

            return RedirectToAction(nameof(Index));
        }
        public async Task<IActionResult> Minus(int cartId)
        {
            ShoppingCartModel cartFromDB = _shoppingCartService.GetById(cartId).GetAwaiter().GetResult().Data!;

            if (cartFromDB != null)
            {
                if (cartFromDB.Count <= 1)
                {
                    await _shoppingCartService.DeleteShoppingCart(cartFromDB.GetDeleteShoppingCartDtoFromShoppingCartModel());
                }
                else
                {
                    cartFromDB.Count -= 1;
                    await _shoppingCartService.UpdateShoppingCart(cartFromDB.GetUpdateShoppingCartDtoFromShoppingCartModel());
                }

            }

            return RedirectToAction(nameof(Index));
        }
        public async Task<IActionResult> Remove(int cartId)
        {
            ShoppingCartModel cartFromDB = _shoppingCartService.GetById(cartId).GetAwaiter().GetResult().Data!;

            if (cartFromDB != null)
            {
                await _shoppingCartService.DeleteShoppingCart(cartFromDB.GetDeleteShoppingCartDtoFromShoppingCartModel());
            }

            return RedirectToAction(nameof(Index));
        }
        private double GetPriceBaseOnQuantity(ShoppingCartModel shoppingCart)
        {
            if (shoppingCart.Count < 50) return shoppingCart.Product!.Price;
            if (shoppingCart.Count < 100) return shoppingCart.Product!.Price50;
            if (shoppingCart.Count >= 100) return shoppingCart.Product!.Price100;
            return 0;
        }
    }
}
