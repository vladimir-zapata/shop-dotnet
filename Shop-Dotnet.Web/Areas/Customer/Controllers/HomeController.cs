﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using Shop_Dotnet.Web.ViewModels;
using Shop_Dotnet.DAL.Entities;
using Shop_Dotnet.Web.Areas.Customer.ViewModels;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Shop_Dotnet.BLL.Contract;
using Shop_Dotnet.BLL.Models;
using Shop_Dotnet.BLL.Extensions;
using Shop_Dotnet.Utility;
using Microsoft.AspNetCore.Http;

namespace Shop_Dotnet.Web.Areas.Customer.Controllers
{
    [Area("Customer")]
    public class HomeController : Controller
    {
        private readonly IProductService _productService;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly ILogger<HomeController> _logger;

        public HomeController(IProductService productService, IShoppingCartService shoppingCartService, ILogger<HomeController> logger)
        {
            _productService = productService;
            _shoppingCartService = shoppingCartService;
            _logger = logger;
        }

        public async Task<IActionResult> Index()
        {
            var products = await this._productService.GetAll();

            HomeVM homeVM = new();

            if (products != null && products.Data != null)
            {
                homeVM.Products = products!.Data;
            }

            return View(homeVM);
        }

        public async Task<IActionResult> Details(int id)
        {
            var product = await this._productService.GetById(id);

            ShoppingCart shoppingCart = new() 
            {
                Product = new Product() 
                {
                    Id = (int)product.Data!.Id,
                    Title = product.Data!.Title,
                    Author = product.Data!.Author,
                    Description = product.Data!.Description,
                    ISBN = product.Data!.ISBN,
                    ListPrice = product.Data!.ListPrice,
                    Price = product.Data!.Price,
                    Price50 = product.Data!.Price50,
                    Price100 = product.Data!.Price100,
                    CategoryId = product.Data!.CategoryId,
                    ImageUrl = product.Data!.ImageUrl,
                    Category = product.Data!.Category == null ? new Category() : new Category()
                    {
                        Name = product.Data!.Category.Name,
                    },
                },
                Count = 1,
                ProductId = id
            };

            return View(shoppingCart);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Details(ShoppingCart shoppingCart)
        {
            var claimsIdentity = (ClaimsIdentity)User.Identity!;
            var userId = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier)!.Value;

            var shoppingCartDto = shoppingCart.GetSaveShoppingCartDtoFromShoppingCart();
            shoppingCartDto.ApplicationUserId = userId;
            shoppingCartDto.RequestUser = userId;

            var serviceResult = await _shoppingCartService.SaveShoppingCart(shoppingCartDto);

            List<ShoppingCartModel> shoppingCarts = _shoppingCartService.GetAllByUserId(userId).GetAwaiter().GetResult().Data!;

            HttpContext.Session.SetInt32(StaticDetails.SessionCart, shoppingCarts.Count());
            TempData["success"] = serviceResult.Message;

            if (serviceResult.Success) return RedirectToAction(nameof(Index));

            return View(shoppingCart);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}