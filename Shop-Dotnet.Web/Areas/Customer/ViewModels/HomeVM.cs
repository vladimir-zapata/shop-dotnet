﻿using Shop_Dotnet.BLL.Models;
using Shop_Dotnet.DAL.Entities;

namespace Shop_Dotnet.Web.Areas.Customer.ViewModels
{
    public class HomeVM
    {
        public List<ProductModel>? Products { get; set; }
    }
}
