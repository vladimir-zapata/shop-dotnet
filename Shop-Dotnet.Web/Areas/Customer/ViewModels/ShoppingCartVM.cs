﻿using Shop_Dotnet.BLL.Models;
using Shop_Dotnet.DAL.Entities;

namespace Shop_Dotnet.Web.Areas.Customer.ViewModels
{
    public class ShoppingCartVM
    {
        public IEnumerable<ShoppingCartModel>? ShoppingCartList { get; set; }
        public OrderHeader OrderHeader { get; set; } = null!;
    }
}
