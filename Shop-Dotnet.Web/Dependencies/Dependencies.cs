﻿using Shop_Dotnet.DAL.Core;

namespace Shop_Dotnet.Web.Dependencies
{
    public static class Dependencies
    {
        public static void AddDependencies(this WebApplicationBuilder builder)
        {
            builder.AddCategoryDependencies();
            builder.AddProductDependencies();
            builder.AddCompanyDependencies();
            builder.AddShoppingCartDependencies();
            builder.AddOrderHeaderDependencies();
            builder.AddOrderDetailDependencies();
            builder.AddApplicationUserDependencies();

            builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
        }
    }
}
