﻿using Shop_Dotnet.BLL.Contract;
using Shop_Dotnet.BLL.Services;
using Shop_Dotnet.DAL.Interfaces;
using Shop_Dotnet.DAL.Repositories;

namespace Shop_Dotnet.Web.Dependencies
{
    public static class OrderDetailDependencies
    {
        public static void AddOrderDetailDependencies(this WebApplicationBuilder builder)
        {
            builder.Services.AddScoped<IOrderDetailService, OrderDetailService>();
        }
    }
}
