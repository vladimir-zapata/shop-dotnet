﻿using Microsoft.AspNetCore.Mvc;
using Shop_Dotnet.BLL.Contract;
using Shop_Dotnet.BLL.Models;
using Shop_Dotnet.Utility;
using System.Security.Claims;

namespace Shop_Dotnet.Web.ViewComponents
{
    public class ShoppingCartViewComponent : ViewComponent
    {
        private readonly IShoppingCartService _shoppingCartService;

        public ShoppingCartViewComponent(IShoppingCartService shoppingCartService)
        {
            _shoppingCartService = shoppingCartService;
        }

        public async Task<IViewComponentResult> InvokeAsync() 
        {
            var claimsIdentity = (ClaimsIdentity)User.Identity!;
            var claim = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier);

            if (claim != null)
            {
                if (HttpContext.Session.GetInt32(StaticDetails.SessionCart) == null) 
                {
                    List<ShoppingCartModel> shoppingCarts = _shoppingCartService.GetAllByUserId(claim.Value).GetAwaiter().GetResult().Data!;
                    HttpContext.Session.SetInt32(StaticDetails.SessionCart, shoppingCarts.Count());
                }

                return View(HttpContext.Session.GetInt32(StaticDetails.SessionCart));
            }
            else 
            {
                HttpContext.Session.Clear();
                return View(0);
            }
        }
    }
}
