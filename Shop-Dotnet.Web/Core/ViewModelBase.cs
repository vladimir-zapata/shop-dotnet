﻿namespace Shop_Dotnet.Web.Core
{
    public abstract class ViewModelBase
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public int DisplayOrder { get; set; }
        public int RequestUser { get; set; }
    }
}
