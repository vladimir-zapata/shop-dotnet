﻿using System.ComponentModel.DataAnnotations;

namespace Shop_Dotnet.DAL.Core
{
    public abstract class BaseEntity : AuditEntity
    {
        [Key]
        public int? Id { get; set; }
    }
}
