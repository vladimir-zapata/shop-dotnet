﻿using Shop_Dotnet.DAL.Context;
using Shop_Dotnet.DAL.Interfaces;
using Shop_Dotnet.DAL.Repositories;

namespace Shop_Dotnet.DAL.Core
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        public ICategoryRepository Category { get; private set; }
        public IProductRepository Product { get; private set; }
        public ICompanyRepository Company { get; private set; }
        public IShoppingCartRepository ShoppingCart { get; private set; }
        public IOrderDetailRepository OrderDetail { get; private set; }
        public IOrderHeaderRepository OrderHeader { get; private set; }
        public IApplicationUserRepository ApplicationUser { get; private set; }

        public UnitOfWork(ApplicationDbContext applicationDbContext)
        {
            this._context = applicationDbContext;
            this.Category = new CategoryRepository(_context);
            this.Product = new ProductRepository(_context);
            this.Company = new CompanyRepository(_context);
            this.ShoppingCart = new ShoppingCartRepository(_context);
            this.OrderDetail = new OrderDetailRepository(_context);
            this.OrderHeader = new OrderHeaderRepository(_context);
            this.ApplicationUser = new ApplicationUserRepository(_context);
        }

        public async Task Commit()
        {
            await this._context.SaveChangesAsync();
        }
    }
}
