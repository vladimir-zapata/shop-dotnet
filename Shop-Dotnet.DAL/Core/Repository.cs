﻿using Microsoft.EntityFrameworkCore;
using Shop_Dotnet.DAL.Context;
using System.Linq.Expressions;


namespace Shop_Dotnet.DAL.Core
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly ApplicationDbContext context;
        private DbSet<TEntity> myEntity;

        public Repository(ApplicationDbContext context)
        {
            this.context = context;
            this.myEntity = context.Set<TEntity>();
        }

        public virtual async Task<bool> ExistsAsync(Expression<Func<TEntity, bool>> filter, string? includeProperties = null)
        {
            return await this.myEntity.AnyAsync(filter);
        }

        public virtual Task<List<TEntity>> GetEntitiesAsync(string? includeProperties = null)
        {
            IQueryable<TEntity> query = myEntity;

            if (!string.IsNullOrEmpty(includeProperties))
            {
                foreach (var includeProp in includeProperties
                    .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProp);
                }
            }

            return query.ToListAsync();
        }

        public virtual async Task<TEntity?> GetEntityAsync(int id, string? includeProperties = null)
        {
            return await this.myEntity.FindAsync(id);

        }

        public virtual async Task RemoveAsync(TEntity entity)
        {
            this.myEntity.Remove(entity);
            await Task.CompletedTask;
        }

        public virtual async Task RemoveAsync(TEntity[] entities)
        {
           this.myEntity.RemoveRange(entities);
            await Task.CompletedTask;
        }

        public virtual async Task SaveAsync(TEntity entity)
        {
            await this.myEntity.AddAsync(entity);
        }

        public virtual async Task SaveAsync(TEntity[] entities)
        {
            await this.myEntity.AddRangeAsync(entities);
        }
        public virtual async Task UpdateAsync(TEntity entity)
        {
            this.context.Update(entity);
            await Task.CompletedTask;
        }

        public virtual async Task UpdateAsync(TEntity[] entities)
        {
            this.context.Update(entities);
            await Task.CompletedTask;
        }
    }
}
