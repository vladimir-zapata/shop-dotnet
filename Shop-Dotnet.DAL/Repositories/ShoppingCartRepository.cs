﻿using Microsoft.EntityFrameworkCore;
using Shop_Dotnet.DAL.Context;
using Shop_Dotnet.DAL.Core;
using Shop_Dotnet.DAL.Entities;
using Shop_Dotnet.DAL.Interfaces;

namespace Shop_Dotnet.DAL.Repositories
{
    public class ShoppingCartRepository : Repository<ShoppingCart>, IShoppingCartRepository
    {
        private readonly ApplicationDbContext _context;

        public ShoppingCartRepository(ApplicationDbContext context): base(context)
        {
            this._context = context;
        }

        public override Task<List<ShoppingCart>> GetEntitiesAsync(string? includeProperties = null)
        {
            IQueryable<ShoppingCart> query = _context.ShoppingCart!;

            if (!string.IsNullOrEmpty(includeProperties))
            {
                foreach (var includeProp in includeProperties
                    .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProp);
                }
            }

            return query.ToListAsync();
        }

        public Task<List<ShoppingCart>> GetAllByUserId(string userId, string? includeProperties = null)
        {
            IQueryable<ShoppingCart> query = _context.ShoppingCart!;

            if (!string.IsNullOrEmpty(includeProperties))
            {
                foreach (var includeProp in includeProperties
                    .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProp);
                }
            }

            return query.Where(x => x.ApplicationUserId == userId).ToListAsync();
        }
    }
}
