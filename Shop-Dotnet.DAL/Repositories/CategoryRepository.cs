﻿using Shop_Dotnet.DAL.Context;
using Shop_Dotnet.DAL.Core;
using Shop_Dotnet.DAL.Entities;
using Shop_Dotnet.DAL.Interfaces;

namespace Shop_Dotnet.DAL.Repositories
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        private readonly ApplicationDbContext _context;

        public CategoryRepository(ApplicationDbContext context) : base(context) 
        {
            this._context = context;
        }
    }
}
