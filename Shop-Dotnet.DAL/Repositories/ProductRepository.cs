﻿using Microsoft.EntityFrameworkCore;
using Shop_Dotnet.DAL.Context;
using Shop_Dotnet.DAL.Core;
using Shop_Dotnet.DAL.Entities;
using Shop_Dotnet.DAL.Interfaces;

namespace Shop_Dotnet.DAL.Repositories
{
    public class ProductRepository : Repository<Product>, IProductRepository
    {
        private readonly ApplicationDbContext _context;

        public ProductRepository(ApplicationDbContext context) : base(context)
        {
            this._context = context;
            _context.Products!.Include(x => x.Category).Include(x => x.CategoryId);
        }

        public override async Task<Product?> GetEntityAsync(int id, string? includeProperties = null)
        {
            IQueryable<Product> query = _context.Products!;
            query = query.Where(x => x.Id == id);

            if (!string.IsNullOrEmpty(includeProperties))
            {
                foreach (var includeProp in includeProperties
                    .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProp);
                }
            }

            return await query.FirstOrDefaultAsync();
        }
    }
}
