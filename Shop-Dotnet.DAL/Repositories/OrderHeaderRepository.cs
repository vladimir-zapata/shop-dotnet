﻿using Microsoft.EntityFrameworkCore;
using Shop_Dotnet.DAL.Context;
using Shop_Dotnet.DAL.Core;
using Shop_Dotnet.DAL.Entities;
using Shop_Dotnet.DAL.Interfaces;

namespace Shop_Dotnet.DAL.Repositories
{
    public class OrderHeaderRepository : Repository<OrderHeader>, IOrderHeaderRepository
    {
        private readonly ApplicationDbContext _context;

        public OrderHeaderRepository(ApplicationDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<OrderHeader?> GetById(int id, string? includeProperties = null)
        {
            IQueryable<OrderHeader> query = _context.OrderHeaders!;
            query = query.Where(x => x.Id == id);

            if (!string.IsNullOrEmpty(includeProperties))
            {
                foreach (var includeProp in includeProperties
                    .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProp);
                }
            }

            return await query.FirstOrDefaultAsync();
        }

        public async Task UpdateStatus(int id, string orderStatus, string? paymentStatus = null)
		{
			var orderFromDB = await _context.OrderHeaders!.FirstOrDefaultAsync(x => x.Id == id);
			if (orderFromDB != null) 
			{
				orderFromDB.OrderStatus = orderStatus;
				if (!string.IsNullOrEmpty(paymentStatus)) 
				{
					orderFromDB.PaymentStatus = paymentStatus;
				}
			}
		}

		public async Task UpdateStripePaymentId(int id, string sessionId, string paymentIntentId)
		{
			var orderFromDB = await _context.OrderHeaders!.FirstOrDefaultAsync(x => x.Id == id);
			if (!string.IsNullOrEmpty(sessionId)) 
			{
				orderFromDB!.SessionId = sessionId;
			}
			if (!string.IsNullOrEmpty(paymentIntentId))
			{
				orderFromDB!.PaymentIntentId = paymentIntentId;
				orderFromDB!.PaymentDate = DateTime.Now;
			}
		}
	}
}
