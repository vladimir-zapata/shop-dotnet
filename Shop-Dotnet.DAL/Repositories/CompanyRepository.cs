﻿using Shop_Dotnet.DAL.Context;
using Shop_Dotnet.DAL.Core;
using Shop_Dotnet.DAL.Entities;
using Shop_Dotnet.DAL.Interfaces;

namespace Shop_Dotnet.DAL.Repositories
{
    public class CompanyRepository : Repository<Company>, ICompanyRepository 
    {
        private readonly ApplicationDbContext _context;

        public CompanyRepository(ApplicationDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
