﻿using Microsoft.EntityFrameworkCore;
using Shop_Dotnet.DAL.Context;
using Shop_Dotnet.DAL.Core;
using Shop_Dotnet.DAL.Entities;
using Shop_Dotnet.DAL.Interfaces;

namespace Shop_Dotnet.DAL.Repositories
{
    public class OrderDetailRepository : Repository<OrderDetail>, IOrderDetailRepository
    {
        private readonly ApplicationDbContext _context;

        public OrderDetailRepository(ApplicationDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
