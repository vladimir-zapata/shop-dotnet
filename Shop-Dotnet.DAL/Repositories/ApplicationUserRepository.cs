﻿using Microsoft.EntityFrameworkCore;
using Shop_Dotnet.DAL.Context;
using Shop_Dotnet.DAL.Core;
using Shop_Dotnet.DAL.Entities;
using Shop_Dotnet.DAL.Interfaces;

namespace Shop_Dotnet.DAL.Repositories
{
    public class ApplicationUserRepository : Repository<ApplicationUser>, IApplicationUserRepository
    {
        private readonly ApplicationDbContext _context;

        public ApplicationUserRepository(ApplicationDbContext context) : base(context)
        {
            this._context = context; 
        }

        public async Task<ApplicationUser?> GetApplicationUserById(string id)
        {
            return await _context.ApplicationUsers!.FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
