﻿using Shop_Dotnet.DAL.Core;
using Shop_Dotnet.DAL.Entities;

namespace Shop_Dotnet.DAL.Interfaces
{
    public interface IProductRepository : IRepository<Product> {}
}
