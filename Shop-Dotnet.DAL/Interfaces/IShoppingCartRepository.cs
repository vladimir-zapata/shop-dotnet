﻿using Shop_Dotnet.DAL.Core;
using Shop_Dotnet.DAL.Entities;

namespace Shop_Dotnet.DAL.Interfaces
{
    public interface IShoppingCartRepository : IRepository<ShoppingCart> 
    {
        Task<List<ShoppingCart>> GetAllByUserId(string applicationUserId, string? includeProperties = null);
    }
}
