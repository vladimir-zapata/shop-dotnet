﻿using Shop_Dotnet.DAL.Core;
using Shop_Dotnet.DAL.Entities;

namespace Shop_Dotnet.DAL.Interfaces
{
    public interface IOrderHeaderRepository : IRepository<OrderHeader> 
    {
        Task<OrderHeader?> GetById(int id, string? includeProperties = null);
        Task UpdateStatus(int id, string orderStatus, string? paymentStatus = null);
		Task UpdateStripePaymentId(int id, string sessionId, string paymentIntentId);
    }
}
