﻿namespace Shop_Dotnet.DAL.DbInitializer
{
    public interface IDbInitializer
    {
        void Initialize();
    }
}
