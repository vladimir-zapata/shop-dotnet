﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Shop_Dotnet.DAL.Migrations
{
    public partial class AddOrderHeaderAndOrderDetailTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "OrderHeaders",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ApplicationUserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    OrderDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ShippingDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    OrderTotal = table.Column<double>(type: "float", nullable: false),
                    OrderStatus = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PaymentStatus = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TrackingNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Carrier = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PaymentDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    PaymentDueDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    PaymentIntentId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    StreetAddress = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    City = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    State = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PostalCode = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CreationUser = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifyUser = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ModifyDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeleteUser = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeleteDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderHeaders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderHeaders_AspNetUsers_ApplicationUserId",
                        column: x => x.ApplicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OrderDetails",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrderHeaderId = table.Column<int>(type: "int", nullable: false),
                    ApplicationUserId = table.Column<int>(type: "int", nullable: false),
                    ProductId = table.Column<int>(type: "int", nullable: false),
                    Count = table.Column<int>(type: "int", nullable: false),
                    Price = table.Column<double>(type: "float", nullable: false),
                    CreationUser = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifyUser = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ModifyDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeleteUser = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeleteDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderDetails_OrderHeaders_ApplicationUserId",
                        column: x => x.ApplicationUserId,
                        principalTable: "OrderHeaders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderDetails_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 12, 10, 2, 28, DateTimeKind.Local).AddTicks(2321), "04ac43fd-50e6-4a75-bb23-9874ab77fddc" });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 12, 10, 2, 28, DateTimeKind.Local).AddTicks(2403), "62befb84-f18c-4feb-bc37-b6eac44f707f" });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 12, 10, 2, 28, DateTimeKind.Local).AddTicks(2407), "f2a24855-f01d-4457-9ff3-bb76d4147c2c" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 12, 10, 2, 28, DateTimeKind.Local).AddTicks(2479), "27b9f984-5908-4eb5-80ad-1820759f9632" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 12, 10, 2, 28, DateTimeKind.Local).AddTicks(2492), "a0c42c18-ebc9-43bc-996b-de7349702efd" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 12, 10, 2, 28, DateTimeKind.Local).AddTicks(2497), "6c220366-cbb2-4a07-841d-4ffa02c79743" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 12, 10, 2, 28, DateTimeKind.Local).AddTicks(2501), "73ab0267-bcb3-4716-bc78-6145f528f51d" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 12, 10, 2, 28, DateTimeKind.Local).AddTicks(2504), "cec28a38-27c6-4809-a3de-a4ed721f17d9" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 12, 10, 2, 28, DateTimeKind.Local).AddTicks(2508), "ce1e36ec-4232-4d0e-9553-ca421b2ef1db" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 12, 10, 2, 28, DateTimeKind.Local).AddTicks(2513), "b6ef28c0-b24f-4de8-b6c6-24d4fdf1e21a" });

            migrationBuilder.CreateIndex(
                name: "IX_OrderDetails_ApplicationUserId",
                table: "OrderDetails",
                column: "ApplicationUserId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderDetails_ProductId",
                table: "OrderDetails",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderHeaders_ApplicationUserId",
                table: "OrderHeaders",
                column: "ApplicationUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OrderDetails");

            migrationBuilder.DropTable(
                name: "OrderHeaders");

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 17, 9, 28, 38, 700, DateTimeKind.Local).AddTicks(1248), "b5ad64de-eb65-49a5-bd3a-e4be02f63550" });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 17, 9, 28, 38, 700, DateTimeKind.Local).AddTicks(1312), "64cb8533-bbff-4c82-864d-3cca0e1d34b4" });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 17, 9, 28, 38, 700, DateTimeKind.Local).AddTicks(1316), "b80eac14-c9a5-46bc-b84b-6393b00d0148" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 17, 9, 28, 38, 700, DateTimeKind.Local).AddTicks(1421), "128ce42a-f27f-4d25-ab35-a4b2de724e56" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 17, 9, 28, 38, 700, DateTimeKind.Local).AddTicks(1433), "923bb5bd-c727-4d1a-8e19-606e942c33c2" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 17, 9, 28, 38, 700, DateTimeKind.Local).AddTicks(1463), "cc601b6c-2ffb-4b31-9d95-1e1fc09647de" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 17, 9, 28, 38, 700, DateTimeKind.Local).AddTicks(1467), "9ba62813-e78e-4dec-9119-851d0e58c232" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 17, 9, 28, 38, 700, DateTimeKind.Local).AddTicks(1470), "976e8f33-99b9-47f0-8981-4d2c2030874c" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 17, 9, 28, 38, 700, DateTimeKind.Local).AddTicks(1473), "25f3b630-9545-4d52-83c4-72e92e58d71a" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 17, 9, 28, 38, 700, DateTimeKind.Local).AddTicks(1476), "0dd78a8a-717b-4066-8f3c-0f8fe51009a4" });
        }
    }
}
