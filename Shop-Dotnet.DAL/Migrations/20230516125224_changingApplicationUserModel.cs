﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Shop_Dotnet.DAL.Migrations
{
    public partial class changingApplicationUserModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "ModifyUser",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DeleteUser",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreationUser",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 16, 8, 52, 23, 815, DateTimeKind.Local).AddTicks(9220), "c6cc3d58-5576-4f68-8d2e-4aff79fc6237" });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 16, 8, 52, 23, 815, DateTimeKind.Local).AddTicks(9309), "93ee36a6-d295-4457-bd91-d81d91edb5be" });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 16, 8, 52, 23, 815, DateTimeKind.Local).AddTicks(9314), "6539efd8-fda6-41bb-8d96-b586c6d92c38" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 16, 8, 52, 23, 815, DateTimeKind.Local).AddTicks(9383), "6d39a001-16f1-4aa0-a837-5bbee89400c0" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 16, 8, 52, 23, 815, DateTimeKind.Local).AddTicks(9396), "0f4cdb60-7b64-4857-b969-508e72756b73" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 16, 8, 52, 23, 815, DateTimeKind.Local).AddTicks(9401), "e386b7bc-90a1-4a02-87ed-c9bdf8788f01" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 16, 8, 52, 23, 815, DateTimeKind.Local).AddTicks(9405), "1b739596-d6a5-480e-b3a9-0ede094566aa" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 16, 8, 52, 23, 815, DateTimeKind.Local).AddTicks(9408), "58ef4003-2ce8-446a-a2e6-d5493e461170" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 16, 8, 52, 23, 815, DateTimeKind.Local).AddTicks(9411), "f4a390c7-733b-4243-b8f1-4b763fdffdc1" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 16, 8, 52, 23, 815, DateTimeKind.Local).AddTicks(9416), "4c6f8cf9-f3a8-456c-9f5c-c599fcd13251" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "ModifyUser",
                table: "AspNetUsers",
                type: "int",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DeleteUser",
                table: "AspNetUsers",
                type: "int",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CreationUser",
                table: "AspNetUsers",
                type: "int",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 16, 8, 43, 33, 396, DateTimeKind.Local).AddTicks(1735), "59c0da3f-5e8f-4334-ac70-cbe19557d49a" });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 16, 8, 43, 33, 396, DateTimeKind.Local).AddTicks(1801), "d9492be8-a9df-4f6b-9e54-b6d55011f079" });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 16, 8, 43, 33, 396, DateTimeKind.Local).AddTicks(1805), "8466ba08-f35c-4b2a-9cd2-be29cc4931d4" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 16, 8, 43, 33, 396, DateTimeKind.Local).AddTicks(2416), "3c517646-3376-456e-94b8-d0f5da91c55e" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 16, 8, 43, 33, 396, DateTimeKind.Local).AddTicks(2462), "7c2b9aef-6934-4c10-a151-1b41ffd13c6f" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 16, 8, 43, 33, 396, DateTimeKind.Local).AddTicks(2468), "a5b68a53-c269-49ae-89ac-1f9a3df17113" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 16, 8, 43, 33, 396, DateTimeKind.Local).AddTicks(2473), "3e1f5496-70dd-46fb-8e98-7a191ee4e19f" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 16, 8, 43, 33, 396, DateTimeKind.Local).AddTicks(2476), "53208218-593d-4f09-a907-61bbcdba0630" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 16, 8, 43, 33, 396, DateTimeKind.Local).AddTicks(2488), "75411aa1-579a-4649-846a-84298274555b" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 16, 8, 43, 33, 396, DateTimeKind.Local).AddTicks(2491), "f4bf83c4-2764-4720-8d27-b5829b5c7f4d" });
        }
    }
}
