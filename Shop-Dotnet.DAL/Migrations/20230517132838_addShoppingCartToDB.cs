﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Shop_Dotnet.DAL.Migrations
{
    public partial class addShoppingCartToDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ShoppingCart",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductId = table.Column<int>(type: "int", nullable: false),
                    Count = table.Column<int>(type: "int", nullable: false),
                    ApplicationUserId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    CreationUser = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifyUser = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ModifyDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeleteUser = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeleteDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShoppingCart", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ShoppingCart_AspNetUsers_ApplicationUserId",
                        column: x => x.ApplicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ShoppingCart_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 17, 9, 28, 38, 700, DateTimeKind.Local).AddTicks(1248), "b5ad64de-eb65-49a5-bd3a-e4be02f63550" });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 17, 9, 28, 38, 700, DateTimeKind.Local).AddTicks(1312), "64cb8533-bbff-4c82-864d-3cca0e1d34b4" });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 17, 9, 28, 38, 700, DateTimeKind.Local).AddTicks(1316), "b80eac14-c9a5-46bc-b84b-6393b00d0148" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 17, 9, 28, 38, 700, DateTimeKind.Local).AddTicks(1421), "128ce42a-f27f-4d25-ab35-a4b2de724e56" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 17, 9, 28, 38, 700, DateTimeKind.Local).AddTicks(1433), "923bb5bd-c727-4d1a-8e19-606e942c33c2" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 17, 9, 28, 38, 700, DateTimeKind.Local).AddTicks(1463), "cc601b6c-2ffb-4b31-9d95-1e1fc09647de" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 17, 9, 28, 38, 700, DateTimeKind.Local).AddTicks(1467), "9ba62813-e78e-4dec-9119-851d0e58c232" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 17, 9, 28, 38, 700, DateTimeKind.Local).AddTicks(1470), "976e8f33-99b9-47f0-8981-4d2c2030874c" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 17, 9, 28, 38, 700, DateTimeKind.Local).AddTicks(1473), "25f3b630-9545-4d52-83c4-72e92e58d71a" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 17, 9, 28, 38, 700, DateTimeKind.Local).AddTicks(1476), "0dd78a8a-717b-4066-8f3c-0f8fe51009a4" });

            migrationBuilder.CreateIndex(
                name: "IX_ShoppingCart_ApplicationUserId",
                table: "ShoppingCart",
                column: "ApplicationUserId");

            migrationBuilder.CreateIndex(
                name: "IX_ShoppingCart_ProductId",
                table: "ShoppingCart",
                column: "ProductId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ShoppingCart");

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 16, 15, 42, 54, 829, DateTimeKind.Local).AddTicks(9707), "f2e24bd7-d171-4d4e-9ef6-effa12ad9e79" });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 16, 15, 42, 54, 829, DateTimeKind.Local).AddTicks(9796), "ae5d0833-fa17-44d3-b0bf-24061730a9b1" });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 16, 15, 42, 54, 829, DateTimeKind.Local).AddTicks(9800), "79480ce3-6593-4951-95e6-8895e1b6334f" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 16, 15, 42, 54, 829, DateTimeKind.Local).AddTicks(9866), "9c4d848e-23bb-4538-bd7f-2129cc3c5398" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 16, 15, 42, 54, 829, DateTimeKind.Local).AddTicks(9880), "c5af8aab-e282-413a-b239-811ad038a17b" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 16, 15, 42, 54, 829, DateTimeKind.Local).AddTicks(9884), "d41f2e8d-6ed6-42e8-be05-cbfe0044febd" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 16, 15, 42, 54, 829, DateTimeKind.Local).AddTicks(9888), "fee7b6ec-27f6-40e9-a327-99cf6eb1f182" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 16, 15, 42, 54, 829, DateTimeKind.Local).AddTicks(9891), "64cf3d69-ec9f-4348-ab7f-f55856cb81ac" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 16, 15, 42, 54, 829, DateTimeKind.Local).AddTicks(9894), "196d65cd-9b6f-43ac-a351-1cce8010efab" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 16, 15, 42, 54, 829, DateTimeKind.Local).AddTicks(9899), "0422d6a3-6aac-4077-bad5-3ff7557a60f0" });
        }
    }
}
