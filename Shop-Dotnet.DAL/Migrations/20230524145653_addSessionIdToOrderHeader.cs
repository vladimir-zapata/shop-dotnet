﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Shop_Dotnet.DAL.Migrations
{
    public partial class addSessionIdToOrderHeader : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SessionId",
                table: "OrderHeaders",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 24, 10, 56, 53, 88, DateTimeKind.Local).AddTicks(822), "9bc3c09b-21ff-481b-bd89-c83ec2d4a325" });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 24, 10, 56, 53, 88, DateTimeKind.Local).AddTicks(889), "ea41d628-bfe3-48f4-9aba-836246462d50" });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 24, 10, 56, 53, 88, DateTimeKind.Local).AddTicks(892), "f53f7e73-c48b-4a74-989f-ec36e1c7d4ae" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 24, 10, 56, 53, 88, DateTimeKind.Local).AddTicks(969), "3b5dac5e-5577-4075-8a0a-725d9c159dfd" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 24, 10, 56, 53, 88, DateTimeKind.Local).AddTicks(980), "b3e94413-7fbe-4cd7-9e35-2f0a470b059f" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 24, 10, 56, 53, 88, DateTimeKind.Local).AddTicks(985), "4cbd339f-c1b1-48cf-a8da-f740e4cda8bf" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 24, 10, 56, 53, 88, DateTimeKind.Local).AddTicks(989), "b239cb8e-1498-4555-a2b3-1c4fe05fe52e" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 24, 10, 56, 53, 88, DateTimeKind.Local).AddTicks(1001), "b72df632-e7b0-47d3-b636-88890d476f3e" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 24, 10, 56, 53, 88, DateTimeKind.Local).AddTicks(1004), "d7aeaf9d-f73b-4753-bf9e-f7512273b955" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 24, 10, 56, 53, 88, DateTimeKind.Local).AddTicks(1007), "b86a5b41-e1bb-49b3-9a7b-d39c38a6b11d" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SessionId",
                table: "OrderHeaders");

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 24, 10, 31, 51, 425, DateTimeKind.Local).AddTicks(2143), "72779548-8ec6-4f3d-95da-368bc791c925" });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 24, 10, 31, 51, 425, DateTimeKind.Local).AddTicks(2201), "5184df8e-e75c-49ba-9b06-17b142fad6e5" });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 24, 10, 31, 51, 425, DateTimeKind.Local).AddTicks(2205), "b567d4fb-3c25-4863-b4d2-670b2d8587e2" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 24, 10, 31, 51, 425, DateTimeKind.Local).AddTicks(2279), "b8401630-8e63-4634-a384-7c17996edd78" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 24, 10, 31, 51, 425, DateTimeKind.Local).AddTicks(2289), "81f00cca-c5cd-41bb-beac-2e5048405046" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 24, 10, 31, 51, 425, DateTimeKind.Local).AddTicks(2302), "8ec3588e-0230-4566-8d22-b0d94e675275" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 24, 10, 31, 51, 425, DateTimeKind.Local).AddTicks(2306), "a2d1c65e-700e-4b56-a543-f0d49b88e64a" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 24, 10, 31, 51, 425, DateTimeKind.Local).AddTicks(2309), "965eb669-bc1c-4607-9c53-f7048bc33b3f" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 24, 10, 31, 51, 425, DateTimeKind.Local).AddTicks(2312), "6b6c1329-532d-494c-9811-e27d0bb880e6" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 24, 10, 31, 51, 425, DateTimeKind.Local).AddTicks(2315), "8434636e-17dc-4ce0-9e38-a6638f71e142" });
        }
    }
}
