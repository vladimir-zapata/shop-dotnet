﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Shop_Dotnet.DAL.Migrations
{
    public partial class ChangingProductSeed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "CreationDate", "CreationUser", "DeleteDate", "DeleteUser", "Deleted", "DisplayOrder", "ModifyDate", "ModifyUser", "Name" },
                values: new object[] { 1, new DateTime(2023, 5, 12, 6, 48, 29, 748, DateTimeKind.Local).AddTicks(504), 1, null, null, false, 1, null, null, "Action" });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "CreationDate", "CreationUser", "DeleteDate", "DeleteUser", "Deleted", "DisplayOrder", "ModifyDate", "ModifyUser", "Name" },
                values: new object[] { 2, new DateTime(2023, 5, 12, 6, 48, 29, 748, DateTimeKind.Local).AddTicks(533), 1, null, null, false, 2, null, null, "SciFi" });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "CreationDate", "CreationUser", "DeleteDate", "DeleteUser", "Deleted", "DisplayOrder", "ModifyDate", "ModifyUser", "Name" },
                values: new object[] { 3, new DateTime(2023, 5, 12, 6, 48, 29, 748, DateTimeKind.Local).AddTicks(535), 1, null, null, false, 3, null, null, "History" });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Author", "CategoryId", "CreationDate", "CreationUser", "DeleteDate", "DeleteUser", "Deleted", "Description", "ISBN", "ImageUrl", "ListPrice", "ModifyDate", "ModifyUser", "Price", "Price100", "Price50", "Title" },
                values: new object[,]
                {
                    { 1, "Billy Spark", 1, new DateTime(2023, 5, 12, 6, 48, 29, 748, DateTimeKind.Local).AddTicks(587), 1, null, null, false, "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ", "SWD9999001", "", 99, null, null, 90, 80, 85, "Fortune of Time" },
                    { 2, "Nancy Hoover", 2, new DateTime(2023, 5, 12, 6, 48, 29, 748, DateTimeKind.Local).AddTicks(594), 1, null, null, false, "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ", "CAW777777701", "", 40, null, null, 30, 20, 25, "Dark Skies" },
                    { 3, "Julian Button", 3, new DateTime(2023, 5, 12, 6, 48, 29, 748, DateTimeKind.Local).AddTicks(596), 1, null, null, false, "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ", "RITO5555501", "", 55, null, null, 50, 35, 40, "Vanish in the Sunset" },
                    { 4, "Abby Muscles", 1, new DateTime(2023, 5, 12, 6, 48, 29, 748, DateTimeKind.Local).AddTicks(598), 1, null, null, false, "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ", "WS3333333301", "", 70, null, null, 65, 55, 60, "Cotton Candy" },
                    { 5, "Ron Parker", 2, new DateTime(2023, 5, 12, 6, 48, 29, 748, DateTimeKind.Local).AddTicks(600), 1, null, null, false, "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ", "SOTJ1111111101", "", 30, null, null, 27, 20, 25, "Rock in the Ocean" },
                    { 6, "Laura Phantom", 3, new DateTime(2023, 5, 12, 6, 48, 29, 748, DateTimeKind.Local).AddTicks(601), 1, null, null, false, "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ", "FOT000000001", "", 25, null, null, 23, 20, 22, "Leaves and Wonders" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "CreationDate", "CreationUser", "DeleteDate", "DeleteUser", "Deleted", "DisplayOrder", "ModifyDate", "ModifyUser", "Name" },
                values: new object[] { 1, new DateTime(2023, 5, 8, 20, 27, 7, 43, DateTimeKind.Local).AddTicks(3837), 1, null, null, false, 1, null, null, "Action" });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "CreationDate", "CreationUser", "DeleteDate", "DeleteUser", "Deleted", "DisplayOrder", "ModifyDate", "ModifyUser", "Name" },
                values: new object[] { 2, new DateTime(2023, 5, 8, 20, 27, 7, 43, DateTimeKind.Local).AddTicks(3848), 1, null, null, false, 2, null, null, "SciFi" });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "CreationDate", "CreationUser", "DeleteDate", "DeleteUser", "Deleted", "DisplayOrder", "ModifyDate", "ModifyUser", "Name" },
                values: new object[] { 3, new DateTime(2023, 5, 8, 20, 27, 7, 43, DateTimeKind.Local).AddTicks(3849), 1, null, null, false, 3, null, null, "History" });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Author", "CategoryId", "CreationDate", "CreationUser", "DeleteDate", "DeleteUser", "Deleted", "Description", "ISBN", "ImageUrl", "ListPrice", "ModifyDate", "ModifyUser", "Price", "Price100", "Price50", "Title" },
                values: new object[] { 1, "Billy Spark", 1, new DateTime(2023, 5, 8, 20, 27, 7, 43, DateTimeKind.Local).AddTicks(3935), 1, null, null, false, "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Suscipit tenetur fugiat dolorum dolore enim. Consequuntur, officiis numquam. Totam, ducimus eaque? Officiis tempore qui alias! Ab odio aliquam consectetur dolor excepturi.", "SW99999901", "", 99, null, null, 90, 80, 85, "Fortune of Time" });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Author", "CategoryId", "CreationDate", "CreationUser", "DeleteDate", "DeleteUser", "Deleted", "Description", "ISBN", "ImageUrl", "ListPrice", "ModifyDate", "ModifyUser", "Price", "Price100", "Price50", "Title" },
                values: new object[] { 2, "Ron Parker", 2, new DateTime(2023, 5, 8, 20, 27, 7, 43, DateTimeKind.Local).AddTicks(3943), 1, null, null, false, "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Suscipit tenetur fugiat dolorum dolore enim. Consequuntur, officiis numquam. Totam, ducimus eaque? Officiis tempore qui alias! Ab odio aliquam consectetur dolor excepturi.", "SDF234299901", "", 30, null, null, 27, 20, 25, "Rock in the Ocean" });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Author", "CategoryId", "CreationDate", "CreationUser", "DeleteDate", "DeleteUser", "Deleted", "Description", "ISBN", "ImageUrl", "ListPrice", "ModifyDate", "ModifyUser", "Price", "Price100", "Price50", "Title" },
                values: new object[] { 3, "Abby Muscles", 2, new DateTime(2023, 5, 8, 20, 27, 7, 43, DateTimeKind.Local).AddTicks(3945), 1, null, null, false, "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Suscipit tenetur fugiat dolorum dolore enim. Consequuntur, officiis numquam. Totam, ducimus eaque? Officiis tempore qui alias! Ab odio aliquam consectetur dolor excepturi.", "WS55234201", "", 70, null, null, 65, 55, 60, "Cotton Candy" });
        }
    }
}
