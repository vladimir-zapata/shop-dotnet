﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Shop_Dotnet.DAL.Migrations
{
    public partial class fixingOrderDetailForeignKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrderDetails_OrderHeaders_ApplicationUserId",
                table: "OrderDetails");

            migrationBuilder.DropIndex(
                name: "IX_OrderDetails_ApplicationUserId",
                table: "OrderDetails");

            migrationBuilder.DropColumn(
                name: "ApplicationUserId",
                table: "OrderDetails");

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 24, 10, 31, 51, 425, DateTimeKind.Local).AddTicks(2143), "72779548-8ec6-4f3d-95da-368bc791c925" });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 24, 10, 31, 51, 425, DateTimeKind.Local).AddTicks(2201), "5184df8e-e75c-49ba-9b06-17b142fad6e5" });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 24, 10, 31, 51, 425, DateTimeKind.Local).AddTicks(2205), "b567d4fb-3c25-4863-b4d2-670b2d8587e2" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 24, 10, 31, 51, 425, DateTimeKind.Local).AddTicks(2279), "b8401630-8e63-4634-a384-7c17996edd78" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 24, 10, 31, 51, 425, DateTimeKind.Local).AddTicks(2289), "81f00cca-c5cd-41bb-beac-2e5048405046" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 24, 10, 31, 51, 425, DateTimeKind.Local).AddTicks(2302), "8ec3588e-0230-4566-8d22-b0d94e675275" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 24, 10, 31, 51, 425, DateTimeKind.Local).AddTicks(2306), "a2d1c65e-700e-4b56-a543-f0d49b88e64a" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 24, 10, 31, 51, 425, DateTimeKind.Local).AddTicks(2309), "965eb669-bc1c-4607-9c53-f7048bc33b3f" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 24, 10, 31, 51, 425, DateTimeKind.Local).AddTicks(2312), "6b6c1329-532d-494c-9811-e27d0bb880e6" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 24, 10, 31, 51, 425, DateTimeKind.Local).AddTicks(2315), "8434636e-17dc-4ce0-9e38-a6638f71e142" });

            migrationBuilder.CreateIndex(
                name: "IX_OrderDetails_OrderHeaderId",
                table: "OrderDetails",
                column: "OrderHeaderId");

            migrationBuilder.AddForeignKey(
                name: "FK_OrderDetails_OrderHeaders_OrderHeaderId",
                table: "OrderDetails",
                column: "OrderHeaderId",
                principalTable: "OrderHeaders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrderDetails_OrderHeaders_OrderHeaderId",
                table: "OrderDetails");

            migrationBuilder.DropIndex(
                name: "IX_OrderDetails_OrderHeaderId",
                table: "OrderDetails");

            migrationBuilder.AddColumn<int>(
                name: "ApplicationUserId",
                table: "OrderDetails",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 15, 58, 34, 955, DateTimeKind.Local).AddTicks(9950), "0d9485d7-2dee-4d8a-9c09-f8e8830eb19c" });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 15, 58, 34, 956, DateTimeKind.Local).AddTicks(6), "a5bb1fa2-8685-4c68-957e-231a6e468967" });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 15, 58, 34, 956, DateTimeKind.Local).AddTicks(10), "5fa91060-1149-476c-a59d-10260d80201d" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 15, 58, 34, 956, DateTimeKind.Local).AddTicks(79), "b4768c95-15bd-47e6-856d-134e43e50007" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 15, 58, 34, 956, DateTimeKind.Local).AddTicks(92), "3ec8f48d-9b90-4388-a77c-96eea1e708b7" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 15, 58, 34, 956, DateTimeKind.Local).AddTicks(97), "e7459230-8be5-467f-9ce8-b832f88dfa63" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 15, 58, 34, 956, DateTimeKind.Local).AddTicks(142), "d1184cfd-564e-4809-a288-f143beb08979" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 15, 58, 34, 956, DateTimeKind.Local).AddTicks(147), "d18edcea-2df8-49fd-a65a-bf406acfd19d" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 15, 58, 34, 956, DateTimeKind.Local).AddTicks(150), "6161782e-5d15-46d9-b196-08f7e21d3584" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 15, 58, 34, 956, DateTimeKind.Local).AddTicks(153), "c01e849a-8203-4d9f-ae51-65634c624fab" });

            migrationBuilder.CreateIndex(
                name: "IX_OrderDetails_ApplicationUserId",
                table: "OrderDetails",
                column: "ApplicationUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_OrderDetails_OrderHeaders_ApplicationUserId",
                table: "OrderDetails",
                column: "ApplicationUserId",
                principalTable: "OrderHeaders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
