﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Shop_Dotnet.DAL.Migrations
{
    public partial class AddProductToDbAndSeed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ISBN = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Author = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ListPrice = table.Column<int>(type: "int", nullable: false),
                    Price = table.Column<int>(type: "int", nullable: false),
                    Price50 = table.Column<int>(type: "int", nullable: false),
                    Price100 = table.Column<int>(type: "int", nullable: false),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreationUser = table.Column<int>(type: "int", nullable: false),
                    ModifyDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ModifyUser = table.Column<int>(type: "int", nullable: true),
                    DeleteUser = table.Column<int>(type: "int", nullable: true),
                    DeleteDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreationDate",
                value: new DateTime(2023, 5, 7, 20, 20, 47, 708, DateTimeKind.Local).AddTicks(4144));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreationDate",
                value: new DateTime(2023, 5, 7, 20, 20, 47, 708, DateTimeKind.Local).AddTicks(4155));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreationDate",
                value: new DateTime(2023, 5, 7, 20, 20, 47, 708, DateTimeKind.Local).AddTicks(4156));

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Author", "CreationDate", "CreationUser", "DeleteDate", "DeleteUser", "Deleted", "Description", "ISBN", "ListPrice", "ModifyDate", "ModifyUser", "Price", "Price100", "Price50", "Title" },
                values: new object[,]
                {
                    { 1, "Billy Spark", new DateTime(2023, 5, 7, 20, 20, 47, 708, DateTimeKind.Local).AddTicks(4243), 1, null, null, false, "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Suscipit tenetur fugiat dolorum dolore enim. Consequuntur, officiis numquam. Totam, ducimus eaque? Officiis tempore qui alias! Ab odio aliquam consectetur dolor excepturi.", "SW99999901", 99, null, null, 90, 80, 85, "Fortune of Time" },
                    { 2, "Ron Parker", new DateTime(2023, 5, 7, 20, 20, 47, 708, DateTimeKind.Local).AddTicks(4246), 1, null, null, false, "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Suscipit tenetur fugiat dolorum dolore enim. Consequuntur, officiis numquam. Totam, ducimus eaque? Officiis tempore qui alias! Ab odio aliquam consectetur dolor excepturi.", "SDF234299901", 30, null, null, 27, 20, 25, "Rock in the Ocean" },
                    { 3, "Abby Muscles", new DateTime(2023, 5, 7, 20, 20, 47, 708, DateTimeKind.Local).AddTicks(4247), 1, null, null, false, "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Suscipit tenetur fugiat dolorum dolore enim. Consequuntur, officiis numquam. Totam, ducimus eaque? Officiis tempore qui alias! Ab odio aliquam consectetur dolor excepturi.", "WS55234201", 70, null, null, 65, 55, 60, "Cotton Candy" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreationDate",
                value: new DateTime(2023, 5, 6, 20, 54, 33, 787, DateTimeKind.Local).AddTicks(4010));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreationDate",
                value: new DateTime(2023, 5, 6, 20, 54, 33, 787, DateTimeKind.Local).AddTicks(4068));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreationDate",
                value: new DateTime(2023, 5, 6, 20, 54, 33, 787, DateTimeKind.Local).AddTicks(4069));
        }
    }
}
