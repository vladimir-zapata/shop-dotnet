﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Shop_Dotnet.DAL.Migrations
{
    public partial class addUserInformationPropertiesToApplicationUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "City",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PostalCode",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "State",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StreetAddress",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 15, 58, 34, 955, DateTimeKind.Local).AddTicks(9950), "0d9485d7-2dee-4d8a-9c09-f8e8830eb19c" });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 15, 58, 34, 956, DateTimeKind.Local).AddTicks(6), "a5bb1fa2-8685-4c68-957e-231a6e468967" });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 15, 58, 34, 956, DateTimeKind.Local).AddTicks(10), "5fa91060-1149-476c-a59d-10260d80201d" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 15, 58, 34, 956, DateTimeKind.Local).AddTicks(79), "b4768c95-15bd-47e6-856d-134e43e50007" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 15, 58, 34, 956, DateTimeKind.Local).AddTicks(92), "3ec8f48d-9b90-4388-a77c-96eea1e708b7" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 15, 58, 34, 956, DateTimeKind.Local).AddTicks(97), "e7459230-8be5-467f-9ce8-b832f88dfa63" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 15, 58, 34, 956, DateTimeKind.Local).AddTicks(142), "d1184cfd-564e-4809-a288-f143beb08979" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 15, 58, 34, 956, DateTimeKind.Local).AddTicks(147), "d18edcea-2df8-49fd-a65a-bf406acfd19d" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 15, 58, 34, 956, DateTimeKind.Local).AddTicks(150), "6161782e-5d15-46d9-b196-08f7e21d3584" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 15, 58, 34, 956, DateTimeKind.Local).AddTicks(153), "c01e849a-8203-4d9f-ae51-65634c624fab" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "City",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "PostalCode",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "State",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "StreetAddress",
                table: "AspNetUsers");

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 12, 10, 2, 28, DateTimeKind.Local).AddTicks(2321), "04ac43fd-50e6-4a75-bb23-9874ab77fddc" });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 12, 10, 2, 28, DateTimeKind.Local).AddTicks(2403), "62befb84-f18c-4feb-bc37-b6eac44f707f" });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 12, 10, 2, 28, DateTimeKind.Local).AddTicks(2407), "f2a24855-f01d-4457-9ff3-bb76d4147c2c" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 12, 10, 2, 28, DateTimeKind.Local).AddTicks(2479), "27b9f984-5908-4eb5-80ad-1820759f9632" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 12, 10, 2, 28, DateTimeKind.Local).AddTicks(2492), "a0c42c18-ebc9-43bc-996b-de7349702efd" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 12, 10, 2, 28, DateTimeKind.Local).AddTicks(2497), "6c220366-cbb2-4a07-841d-4ffa02c79743" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 12, 10, 2, 28, DateTimeKind.Local).AddTicks(2501), "73ab0267-bcb3-4716-bc78-6145f528f51d" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 12, 10, 2, 28, DateTimeKind.Local).AddTicks(2504), "cec28a38-27c6-4809-a3de-a4ed721f17d9" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 12, 10, 2, 28, DateTimeKind.Local).AddTicks(2508), "ce1e36ec-4232-4d0e-9553-ca421b2ef1db" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreationDate", "CreationUser" },
                values: new object[] { new DateTime(2023, 5, 23, 12, 10, 2, 28, DateTimeKind.Local).AddTicks(2513), "b6ef28c0-b24f-4de8-b6c6-24d4fdf1e21a" });
        }
    }
}
