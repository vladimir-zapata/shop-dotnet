﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Shop_Dotnet.DAL.Core;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Shop_Dotnet.DAL.Entities
{
    public class OrderDetail : BaseEntity
    {
        [Required]
        public int OrderHeaderId { get; set; }
        [ForeignKey("OrderHeaderId")]
        [ValidateNever]
        public OrderHeader OrderHeader { get; set; } = null!;

        [Required]
        public int ProductId { get; set; }
        [ForeignKey("ProductId")]
        [ValidateNever]
        public Product Product { get; set; } = null!;

        public int Count { get; set; }
        public double Price { get; set; }
    }
}
