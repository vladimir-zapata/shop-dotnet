﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Shop_Dotnet.DAL.Core;

namespace Shop_Dotnet.DAL.Entities
{
    public class Category : BaseEntity
    {
        [Required]
        [MaxLength(30)]
        [DisplayName("Category Name")]
        public string? Name { get; set; }

        [Range(1, 100, ErrorMessage = "Display Order field must be a number between 1-100")]
        [DisplayName("Display Order")]
        public int DisplayOrder { get; set; }
    }
}
