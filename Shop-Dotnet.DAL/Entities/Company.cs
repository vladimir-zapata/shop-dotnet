﻿using Shop_Dotnet.DAL.Core;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace Shop_Dotnet.DAL.Entities
{
    public class Company : BaseEntity
    {
        [Required]
        [MaxLength(30)]
        [DisplayName("Company Name")]
        public string? Name { get; set; }
        public string? StreetAddress { get; set; }
        public string? City { get; set; }
        public string? State { get; set; }
        public string? PostalCode { get; set; }
        public string? PhoneNumber { get; set; }
    }
}
