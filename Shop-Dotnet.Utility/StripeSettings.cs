﻿namespace Shop_Dotnet.Utility
{
	public class StripeSettings
	{
		public string? SecretKey { get; set; }
		public string? PublishableKey { get; set; }
	}
}
