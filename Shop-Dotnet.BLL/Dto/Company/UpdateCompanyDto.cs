﻿using Shop_Dotnet.BLL.Core;

namespace Shop_Dotnet.BLL.Dto.Company
{
    public class UpdateCompanyDto : BaseDto
    {
        public string? Name { get; set; }
        public string? StreetAddress { get; set; }
        public string? City { get; set; }
        public string? State { get; set; }
        public string? PostalCode { get; set; }
        public string? PhoneNumber { get; set; }
    }
}
