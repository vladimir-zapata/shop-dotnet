﻿using Shop_Dotnet.BLL.Core;

namespace Shop_Dotnet.BLL.Dto.Product
{
    public class SaveProductDto : BaseDto
    {
        public string? Title { get; set; }
        public string? Description { get; set; }
        public string? ISBN { get; set; }
        public string? Author { get; set; }
        public int ListPrice { get; set; }
        public int Price { get; set; }
        public int Price50 { get; set; }
        public int Price100 { get; set; }
        public int CategoryId { get; set;}
        public string? ImageUrl { get; set;}
    }
}
