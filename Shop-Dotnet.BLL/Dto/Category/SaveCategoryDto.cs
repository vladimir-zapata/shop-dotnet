﻿using Shop_Dotnet.BLL.Core;

namespace Shop_Dotnet.BLL.Dto.Category
{
    public class SaveCategoryDto : BaseDto
    {
        public string? Name { get; set; }
        public int DisplayOrder { get; set; }
    }
}
