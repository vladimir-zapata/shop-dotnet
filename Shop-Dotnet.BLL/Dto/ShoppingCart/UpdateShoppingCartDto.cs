﻿using Shop_Dotnet.BLL.Core;
using System.ComponentModel.DataAnnotations;

namespace Shop_Dotnet.BLL.Dto.ShoppingCart
{
    public class UpdateShoppingCartDto : BaseDto
    {
        public int ProductId { get; set; }

        [Range(1, 1000, ErrorMessage = "Please enter a value between 1 and 1000")]
        public int Count { get; set; }

        public string? ApplicationUserId { get; set; }
    }
}
