﻿namespace Shop_Dotnet.BLL.Core
{
    public interface IBaseService
    {
        Task<ServiceResult> GetAll();
        Task<ServiceResult> GetById(int id);
    }
}
