﻿using System.ComponentModel.DataAnnotations;

namespace Shop_Dotnet.BLL.Core
{
    public abstract class BaseDto
    {
        public int? Id { get; set; }
        [Required]
        public string? RequestUser { get; set; }
    }
}
