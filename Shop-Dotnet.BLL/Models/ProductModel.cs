﻿using Shop_Dotnet.DAL.Entities;

namespace Shop_Dotnet.BLL.Models
{
    public class ProductModel
    {
        public int Id { get; set; }
        public string? Title { get; set; }
        public string? Description { get; set; }
        public string? ISBN { get; set; }
        public string? Author { get; set; }
        public int ListPrice { get; set; }
        public int Price { get; set; }
        public int Price50 { get; set; }
        public int Price100 { get; set; }
        public int CategoryId { get; set; }
        public CategoryModel? Category { get; set; }
        public string? ImageUrl { get; set; }
    }
}
