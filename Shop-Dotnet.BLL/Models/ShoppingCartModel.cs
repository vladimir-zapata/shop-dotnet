﻿using Shop_Dotnet.DAL.Entities;
using System.ComponentModel.DataAnnotations;

namespace Shop_Dotnet.BLL.Models
{
    public class ShoppingCartModel
    {
        public int? Id { get; set; }
        public int? ProductId { get; set; }
        public Product? Product { get; set; }
        public int Count { get; set; }
        public string? ApplicationUserId { get; set; }
        public double Price { get; set; }
    }
}
