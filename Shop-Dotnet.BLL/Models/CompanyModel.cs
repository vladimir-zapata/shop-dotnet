﻿namespace Shop_Dotnet.BLL.Models
{
    public class CompanyModel
    {
        public int? Id { get; set; } 
        public string? Name { get; set; }
        public string? StreetAddress { get; set; }
        public string? City { get; set; }
        public string? State { get; set; }
        public string? PostalCode { get; set; }
        public string? PhoneNumber { get; set; }
    }
}
