﻿using Shop_Dotnet.BLL.Core;
using Shop_Dotnet.BLL.Dto.Category;

namespace Shop_Dotnet.BLL.Contract
{
    public interface ICategoryService : IBaseService
    {
        Task<ServiceResult> SaveCategory(SaveCategoryDto saveCategoryDto);
        Task<ServiceResult> UpdateCategory(UpdateCategoryDto updateCategoryDto);
        Task<ServiceResult> DeleteCategory(DeleteCategoryDto deleteCategoryDto);
    }
}
