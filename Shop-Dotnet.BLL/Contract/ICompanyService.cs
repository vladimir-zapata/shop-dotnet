﻿using Shop_Dotnet.BLL.Core;
using Shop_Dotnet.BLL.Dto.Company;
using Shop_Dotnet.BLL.Dto.Product;

namespace Shop_Dotnet.BLL.Contract
{
    public interface ICompanyService : IBaseService
    {
        Task<ServiceResult> SaveCompany(SaveCompanyDto saveCompanyDto);
        Task<ServiceResult> UpdateCompany(UpdateCompanyDto updateCompanyDto);
        Task<ServiceResult> DeleteCompany(DeleteCompanyDto deleteCompanyDto);

    }
}
