﻿using Shop_Dotnet.BLL.Core;
using Shop_Dotnet.DAL.Entities;

namespace Shop_Dotnet.BLL.Contract
{
    public interface IOrderDetailService
    {
        Task<ServiceResult> GetAll();
        Task<ServiceResult> SaveOrderDetail(OrderDetail orderDetail);
    }
}
