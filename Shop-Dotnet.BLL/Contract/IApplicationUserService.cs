﻿using Shop_Dotnet.BLL.Core;

namespace Shop_Dotnet.BLL.Contract
{
    public interface IApplicationUserService 
    {
        Task<ServiceResult> GetAll();
        Task<ServiceResult> GetById(string id);
    }
}
