﻿using Shop_Dotnet.BLL.Core;
using Shop_Dotnet.BLL.Dto.ShoppingCart;
using Shop_Dotnet.DAL.Entities;

namespace Shop_Dotnet.BLL.Contract
{
    public interface IShoppingCartService : IBaseService
    {
        Task<ServiceResult> GetAllByUserId(string productId);
        Task<ServiceResult> SaveShoppingCart(SaveShoppingCartDto saveShoppingCartDto);
        Task<ServiceResult> UpdateShoppingCart(UpdateShoppingCartDto updateShoppingCartDto);
        Task<ServiceResult> DeleteShoppingCart(DeleteShoppingCartDto deleteShoppingCartDto);
        Task<ServiceResult> DeleteAllShoppingCart(string shoppingCarts);
    }
}
