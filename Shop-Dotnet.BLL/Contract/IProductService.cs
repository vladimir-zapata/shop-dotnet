﻿using Shop_Dotnet.BLL.Core;
using Shop_Dotnet.BLL.Dto.Product;

namespace Shop_Dotnet.BLL.Contract
{
    public interface IProductService : IBaseService
    {
        Task<ServiceResult> SaveProduct(SaveProductDto saveProductDto);
        Task<ServiceResult> UpdateProduct(UpdateProductDto updateProductDto);
        Task<ServiceResult> DeleteProduct(DeleteProductDto deleteProductDto);
    }
}
