﻿using Shop_Dotnet.BLL.Core;
using Shop_Dotnet.BLL.Models;
using Shop_Dotnet.DAL.Entities;

namespace Shop_Dotnet.BLL.Contract
{
    public interface IOrderHeaderService : IBaseService
    {
        Task<ServiceResult> GetAllByApplicationUserId(string applicationUserId);
        Task<ServiceResult> SaveOrderHeader(OrderHeader orderHeader);
        Task<ServiceResult> ShipOrder(OrderHeaderModel orderHeaderModel);
        Task<ServiceResult> UpdateOrderHeader(OrderHeaderModel orderHeaderModel);
        Task<ServiceResult> UpdateStatus(int id, string orderStatus, string? paymentStatus = null);
        Task<ServiceResult> UpdateStripePaymentId(int id, string sessionId, string paymentIntentId);
    }
}
