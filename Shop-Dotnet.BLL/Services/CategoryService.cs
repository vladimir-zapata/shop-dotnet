﻿using Microsoft.Extensions.Logging;
using Shop_Dotnet.BLL.Contract;
using Shop_Dotnet.BLL.Core;
using Shop_Dotnet.BLL.Dto.Category;
using Shop_Dotnet.BLL.Dto.Product;
using Shop_Dotnet.BLL.Extensions;
using Shop_Dotnet.DAL.Core;

namespace Shop_Dotnet.BLL.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger _logger;

        public CategoryService(IUnitOfWork unitOfWork, ILogger<CategoryService> logger)
        {
            this._unitOfWork = unitOfWork;
            this._logger = logger;
        }

        public async Task<ServiceResult> GetAll()
        {
            ServiceResult result = new ServiceResult();

            try
            {
                _logger.LogInformation("Getting categories");

                var categories = await _unitOfWork.Category.GetEntitiesAsync();

                var categoryList = categories
                    .Where(x => !x.Deleted)
                    .Select(x => x.GetCategoryModelFromCategory())
                    .ToList();

                result.Message = "Categories succesfully displayed";
                result.Data = categoryList;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Error getting los categories";
                _logger.LogError($"{result.Message}", ex.ToString());
            }

            return result;
        }
        public async Task<ServiceResult> GetById(int id)
        {
            ServiceResult result = new ServiceResult();

            if(id == 0) { result.Success = false; result.Message = "Id can't be null"; return result; };

            try
            {
                _logger.LogInformation($"Getting category by id: {id}");

                var category = await _unitOfWork.Category.GetEntityAsync(id);

                if (category == null) 
                {
                    result.Message = "Category not found";
                    return result;
                }

                result.Data = category?.GetCategoryModelFromCategory();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Error getting category by id";
                _logger.LogError($"{result.Message}", ex.ToString());
            }

            return result;
        }
        public async Task<ServiceResult> SaveCategory(SaveCategoryDto saveCategoryDto)
        {
            ServiceResult result = new ServiceResult();

            try
            {
                _logger.LogInformation($"Saving category: {saveCategoryDto.Name}");

                var category = saveCategoryDto.GetCategoryFromSaveCategoryDto();
                category.CreationUser = saveCategoryDto.RequestUser;

                await this._unitOfWork.Category.SaveAsync(category);
                await this._unitOfWork.Commit();

                result.Message = "Category successfully saved";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Error saving category";
                _logger.LogError($"{result.Message}", ex.ToString());
            }

            return result;
        }
        public async Task<ServiceResult> UpdateCategory(UpdateCategoryDto updateCategoryDto)
        {
            ServiceResult result = new ServiceResult();

            if (updateCategoryDto.Id == null)
            {
                result.Success = false;
                result.Message = "Id can't be null";
                return result;
            }

            try
            {
                _logger.LogInformation($"Updating category id: {updateCategoryDto.Id}");

                var category = await this._unitOfWork.Category.GetEntityAsync(updateCategoryDto.Id.Value);

                if (category == null) 
                {
                    result.Success = true;
                    result.Message = "Category not found";
                    return result;
                }

                category.Name = updateCategoryDto.Name;
                category.ModifyDate = DateTime.Now;
                category.ModifyUser = updateCategoryDto.RequestUser;
                category.DisplayOrder = updateCategoryDto.DisplayOrder;

                await this._unitOfWork.Category.UpdateAsync(category);
                await this._unitOfWork.Commit();

                result.Message = "Category successfully updated";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Error updating category";
                _logger.LogError($"{result.Message}", ex.ToString());
            }

            return result;
        }
        public async Task<ServiceResult> DeleteCategory(DeleteCategoryDto deleteCategoryDto)
        {
            ServiceResult result = new ServiceResult();

            if (deleteCategoryDto.Id == null || deleteCategoryDto.Id == 0)
            {
                result.Success = false;
                result.Message = "Id can't be null";
                return result;
            }

            try
            {
                _logger.LogInformation($"Deleting category id: {deleteCategoryDto.Id}");

                var category = await this._unitOfWork.Category.GetEntityAsync(deleteCategoryDto.Id.Value);

                if (category == null)
                {
                    result.Success = true;
                    result.Message = "Category not found";
                    return result;
                }

                category.DeleteDate = DateTime.Now;
                category.DeleteUser = deleteCategoryDto.RequestUser;
                category.Deleted = true;

                await this._unitOfWork.Category.UpdateAsync(category);
                await this._unitOfWork.Commit();

                result.Message = "Category successfully deleted";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Error deleting category";
                _logger.LogError($"{result.Message}", ex.ToString());
            }

            return result;
        }
    }
}
