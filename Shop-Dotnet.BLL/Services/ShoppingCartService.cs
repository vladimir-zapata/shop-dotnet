﻿using Microsoft.Extensions.Logging;
using Shop_Dotnet.BLL.Contract;
using Shop_Dotnet.BLL.Core;
using Shop_Dotnet.BLL.Dto.ShoppingCart;
using Shop_Dotnet.BLL.Extensions;
using Shop_Dotnet.DAL.Core;
using Shop_Dotnet.DAL.Entities;

namespace Shop_Dotnet.BLL.Services
{
    public class ShoppingCartService : IShoppingCartService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger _logger;

        public ShoppingCartService(IUnitOfWork unitOfWork, ILogger<ShoppingCartService> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }
        public async Task<ServiceResult> GetAll()
        {
            ServiceResult result = new ServiceResult();

            try
            {
                _logger.LogInformation("Getting shopping carts");

                var shoppingCarts = await _unitOfWork.ShoppingCart.GetEntitiesAsync(includeProperties: "Product");

                var shoppingCartList = shoppingCarts
                    .Where(x => !x.Deleted)
                    .Select(x => x.GetShoppingCartModelFromShoppingCart())
                    .ToList();

                result.Message = "Shopping carts succesfully displayed";
                result.Data = shoppingCartList;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Error getting los categories";
                _logger.LogError($"{result.Message}", ex.ToString());
            }

            return result;
        }
        public async Task<ServiceResult> GetAllByUserId(string userId)
        {
            ServiceResult result = new ServiceResult();

            try
            {
                _logger.LogInformation("Getting shopping carts");

                var shoppingCarts = await _unitOfWork.ShoppingCart.GetAllByUserId(userId, includeProperties: "Product");

                var shoppingCartList = shoppingCarts
                    .Where(x => !x.Deleted)
                    .Select(x => x.GetShoppingCartModelFromShoppingCart())
                    .ToList();

                result.Message = "Shopping carts succesfully displayed";
                result.Data = shoppingCartList;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Error getting shopping cart";
                _logger.LogError($"{result.Message}", ex.ToString());
            }

            return result;
        }
        public async Task<ServiceResult> GetById(int id)
        {
            ServiceResult result = new ServiceResult();

            if (id == 0) { result.Success = false; result.Message = "Id can't be null"; return result; };

            try
            {
                _logger.LogInformation($"Getting shopping cart by id: {id}");

                var shoppingCart = await _unitOfWork.ShoppingCart.GetEntityAsync(id);

                if (shoppingCart == null)
                {
                    result.Message = "Shopping cart not found";
                    return result;
                }

                result.Data = shoppingCart?.GetShoppingCartModelFromShoppingCart();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Error getting shopping cart by id";
                _logger.LogError($"{result.Message}", ex.ToString());
            }

            return result;
        }
        public async Task<ServiceResult> SaveShoppingCart(SaveShoppingCartDto saveShoppingCartDto)
        {
            ServiceResult result = new ServiceResult();

            try
            {
                _logger.LogInformation($"Saving shopping cart");
                var shoppingCart = saveShoppingCartDto.GetShoppingCartFromSaveShoppingCartDto();

                var cartFromDB = _unitOfWork.ShoppingCart
                    .GetEntitiesAsync()
                    .GetAwaiter()
                    .GetResult()
                    .FirstOrDefault(
                        x => !x.Deleted &&
                        x.ApplicationUserId == shoppingCart.ApplicationUserId &&
                        x.ProductId == shoppingCart.ProductId
                    );

                if (cartFromDB != null)
                {
                    cartFromDB.Count += shoppingCart.Count;
                } 
                else 
                {
                    shoppingCart.CreationUser = saveShoppingCartDto.RequestUser;
                    await this._unitOfWork.ShoppingCart.SaveAsync(shoppingCart);
                }

                await this._unitOfWork.Commit();

                result.Message = "Shopping cart successfully saved";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Error saving shopping cart";
                _logger.LogError($"{result.Message}", ex.ToString());
            }

            return result;
        }
        public async Task<ServiceResult> UpdateShoppingCart(UpdateShoppingCartDto updateShoppingCartDto)
        {
            ServiceResult result = new ServiceResult();

            if (updateShoppingCartDto.Id == null || updateShoppingCartDto.Id == 0)
            {
                result.Success = false;
                result.Message = "Id can't be null";
                return result;
            }

            try
            {
                _logger.LogInformation($"Updating shopping cart id: {updateShoppingCartDto.Id}");

                var shoppingCart = await this._unitOfWork.ShoppingCart.GetEntityAsync(updateShoppingCartDto.Id.Value);

                if (shoppingCart == null)
                {
                    result.Success = true;
                    result.Message = "Shopping cart not found";
                    return result;
                }

                shoppingCart.ProductId = updateShoppingCartDto.ProductId;
                shoppingCart.ApplicationUserId = updateShoppingCartDto.ApplicationUserId;
                shoppingCart.Count = updateShoppingCartDto.Count;
                shoppingCart.ModifyDate = DateTime.Now;
                shoppingCart.ModifyUser = updateShoppingCartDto.RequestUser;

                await _unitOfWork.ShoppingCart.UpdateAsync(shoppingCart);
                await _unitOfWork.Commit();

                result.Message = "Shopping cart successfully updated";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Error updating shopping cart";
                _logger.LogError($"{result.Message}", ex.ToString());
            }

            return result;
        }
        public async Task<ServiceResult> DeleteShoppingCart(DeleteShoppingCartDto deleteShoppingCartDto)
        {
            ServiceResult result = new ServiceResult();

            if (deleteShoppingCartDto.Id == null || deleteShoppingCartDto.Id == 0)
            {
                result.Success = false;
                result.Message = "Id can't be null";
                return result;
            }

            try
            {
                _logger.LogInformation($"Deleting shopping cart id: {deleteShoppingCartDto.Id}");

                var shoppingCart = await this._unitOfWork.ShoppingCart.GetEntityAsync(deleteShoppingCartDto.Id.Value);

                if (shoppingCart == null)
                {
                    result.Success = true;
                    result.Message = "Shopping cart not found";
                    return result;
                }

                shoppingCart.DeleteDate = DateTime.Now;
                shoppingCart.DeleteUser = deleteShoppingCartDto.RequestUser;
                shoppingCart.Deleted = true;

                await this._unitOfWork.ShoppingCart.UpdateAsync(shoppingCart);
                await this._unitOfWork.Commit();

                result.Message = "Shopping cart successfully deleted";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Error deleting shopping cart";
                _logger.LogError($"{result.Message}", ex.ToString());
            }

            return result;
        }
		public async Task<ServiceResult> DeleteAllShoppingCart(string userId)
		{
			ServiceResult result = new ServiceResult();

			try
			{
				_logger.LogInformation($"Deleting all shopping carts");

				var shoppingCarts = await _unitOfWork.ShoppingCart.GetAllByUserId(userId);

                await this._unitOfWork.ShoppingCart.RemoveAsync(shoppingCarts.ToArray());
                await this._unitOfWork.Commit();

                result.Message = "Shopping carts sucessfully deleted";
			}
			catch (Exception ex)
			{
				result.Success = false;
				result.Message = "Error getting shopping cart by id";
				_logger.LogError($"{result.Message}", ex.ToString());
			}

			return result;
		}
	}
}
