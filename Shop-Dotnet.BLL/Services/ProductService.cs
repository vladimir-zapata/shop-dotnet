﻿using Microsoft.Extensions.Logging;
using Shop_Dotnet.BLL.Contract;
using Shop_Dotnet.BLL.Core;
using Shop_Dotnet.BLL.Dto.Product;
using Shop_Dotnet.BLL.Extensions;
using Shop_Dotnet.DAL.Core;

namespace Shop_Dotnet.BLL.Services
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger _logger;

        public ProductService(IUnitOfWork unitOfWork, ILogger<ProductService> logger)
        {
            this._unitOfWork = unitOfWork;
            this._logger = logger;
        }
        public async Task<ServiceResult> GetAll()
        {
            ServiceResult result = new ServiceResult();

            try
            {
                _logger.LogInformation("Getting products");

                var products = await _unitOfWork.Product.GetEntitiesAsync(includeProperties: "Category");

                var productsList = products
                    .Where(x => !x.Deleted)
                    .Select(x => x.GetProductModelFromProduct())
                    .ToList();

                result.Message = "Products succesfully displayed";
                result.Data = productsList;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Error getting products";
                _logger.LogError($"{result.Message}", ex.ToString());
            }

            return result;
        }
        public async Task<ServiceResult> GetById(int id)
        {
            ServiceResult result = new ServiceResult();

            if (id == 0) { result.Success = false; result.Message = "Id can't be null"; return result; };

            try
            {
                _logger.LogInformation($"Getting product by id: {id}");

                var product = await _unitOfWork.Product.GetEntityAsync(id, "Category");

                if (product == null)
                {
                    result.Message = "Product not found";
                    return result;
                }

                result.Data = product?.GetProductModelFromProduct();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Error getting product by id";
                _logger.LogError($"{result.Message}", ex.ToString());
            }

            return result;
        }
        public async Task<ServiceResult> SaveProduct(SaveProductDto saveProductDto)
        {
            ServiceResult result = new ServiceResult();

            try
            {
                _logger.LogInformation($"Saving product: {saveProductDto.Title}");

                var product = saveProductDto.GetProductFromSaveProductDto();

                await this._unitOfWork.Product.SaveAsync(product);
                await this._unitOfWork.Commit();

                result.Message = "Product successfully saved";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Error saving Product";
                _logger.LogError($"{result.Message}", ex.ToString());
            }

            return result;
        }
        public async Task<ServiceResult> UpdateProduct(UpdateProductDto updateProductDto)
        {
            ServiceResult result = new ServiceResult();

            if(updateProductDto.Id == null || updateProductDto.Id == 0) 
            { 
                result.Success = false; 
                result.Message = "Id can't be null"; 
                return result; 
            }

            try
            {
                _logger.LogInformation($"Updating product id: {updateProductDto.Id}");

                var product = await this._unitOfWork.Product.GetEntityAsync(updateProductDto.Id.Value);

                if (product == null)
                {
                    result.Success = true;
                    result.Message = "Product not found";
                    return result;
                }

                product.Title = updateProductDto.Title;
                product.Author = updateProductDto.Author;
                product.Description = updateProductDto.Description;
                product.ISBN = updateProductDto.ISBN;
                product.ListPrice = updateProductDto.ListPrice;
                product.Price = updateProductDto.Price;
                product.Price50 = updateProductDto.Price50;
                product.Price100 = updateProductDto.Price100;
                product.CategoryId = updateProductDto.CategoryId;
                product.ImageUrl = updateProductDto.ImageUrl;
                product.ModifyDate = DateTime.Now;
                product.ModifyUser = updateProductDto.RequestUser;

                await this._unitOfWork.Product.UpdateAsync(product);
                await this._unitOfWork.Commit();

                result.Message = "Product successfully updated";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Error updating product";
                _logger.LogError($"{result.Message}", ex.ToString());
            }

            return result;
        }
        public async Task<ServiceResult> DeleteProduct(DeleteProductDto deleteProductDto)
        {
            ServiceResult result = new ServiceResult();

            if (deleteProductDto.Id == null || deleteProductDto.Id == 0)
            {
                result.Success = false;
                result.Message = "Id can't be null";
                return result;
            }

            try
            {
                _logger.LogInformation($"Deleting product id: {deleteProductDto.Id}");

                var product = await this._unitOfWork.Product.GetEntityAsync(deleteProductDto.Id.Value);

                if (product == null)
                {
                    result.Success = true;
                    result.Message = "Product not found";
                    return result;
                }

                product.DeleteDate = DateTime.Now;
                product.DeleteUser = deleteProductDto.RequestUser;
                product.Deleted = true;

                await this._unitOfWork.Product.UpdateAsync(product);
                await this._unitOfWork.Commit();

                result.Message = "Product successfully deleted";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Error deleting product";
                _logger.LogError($"{result.Message}", ex.ToString());
            }

            return result;
        }
    }
}
