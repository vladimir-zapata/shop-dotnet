﻿using Microsoft.Extensions.Logging;
using Shop_Dotnet.BLL.Contract;
using Shop_Dotnet.BLL.Core;
using Shop_Dotnet.BLL.Extensions;
using Shop_Dotnet.DAL.Core;
using Shop_Dotnet.DAL.Entities;

namespace Shop_Dotnet.BLL.Services
{
    public class ApplicationUserService : IApplicationUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger _logger;

        public ApplicationUserService(IUnitOfWork unitOfWork, ILogger<ApplicationUserService> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        public async Task<ServiceResult> GetAll()
        {
            ServiceResult result = new ServiceResult();

            try
            {
                _logger.LogInformation("Getting all users");

                var users = await _unitOfWork.ApplicationUser.GetEntitiesAsync();

                result.Message = "Users succesfully displayed";
                result.Data = users.Select(x => x.GetApplicationUserModelFromApplicationUser()).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Error getting user";
                _logger.LogError($"{result.Message}", ex.ToString());
            }

            return result;
        }

        public async Task<ServiceResult> GetById(string id)
        {
            ServiceResult result = new ServiceResult();

            if (id == null) { result.Success = false; result.Message = "Id can't be null"; return result; };

            try
            {
                _logger.LogInformation($"Getting user by id: {id}");

                var user = await _unitOfWork.ApplicationUser.GetApplicationUserById(id);

                if (user == null)
                {
                    result.Message = "User not found";
                    return result;
                }

                //result.Data = user.GetApplicationUserModelFromApplicationUser();
                result.Data = user;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Error getting user by id";
                _logger.LogError($"{result.Message}", ex.ToString());
            }

            return result;
        }
    }
}
