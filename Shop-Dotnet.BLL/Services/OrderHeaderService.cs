﻿using Microsoft.Extensions.Logging;
using Shop_Dotnet.BLL.Contract;
using Shop_Dotnet.BLL.Core;
using Shop_Dotnet.BLL.Extensions;
using Shop_Dotnet.BLL.Models;
using Shop_Dotnet.DAL.Core;
using Shop_Dotnet.DAL.Entities;
using Shop_Dotnet.Utility;

namespace Shop_Dotnet.BLL.Services
{
    public class OrderHeaderService : IOrderHeaderService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger _logger;

        public OrderHeaderService(IUnitOfWork unitOfWork, ILogger<OrderHeaderService> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        public async Task<ServiceResult> GetAll()
        {
            ServiceResult result = new ServiceResult();

            try
            {
                _logger.LogInformation("Getting order headers");

                var categories = await _unitOfWork.OrderHeader.GetEntitiesAsync("ApplicationUser");

                var categoryList = categories
                    .Where(x => !x.Deleted)
                    .Select(x => x.GetOrderHeaderModelFromOrderHeader())
                    .ToList();

                result.Message = "Order headers succesfully displayed";
                result.Data = categoryList;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Error getting order headers";
                _logger.LogError($"{result.Message}", ex.ToString());
            }

            return result;
        }

        public async Task<ServiceResult> GetAllByApplicationUserId(string applicationUserId)
        {
            ServiceResult result = new ServiceResult();

            try
            {
                _logger.LogInformation("Getting order headers");

                var categories = await _unitOfWork.OrderHeader.GetEntitiesAsync("ApplicationUser");

                var categoryList = categories
                    .Where(x => !x.Deleted)
                    .Select(x => x.GetOrderHeaderModelFromOrderHeader())
                    .ToList();

                result.Message = "Order headers succesfully displayed";
                result.Data = categoryList.Where(x => x.ApplicationUserId == applicationUserId).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Error getting order headers";
                _logger.LogError($"{result.Message}", ex.ToString());
            }

            return result;
        }

        public async Task<ServiceResult> GetById(int id)
        {
            ServiceResult result = new ServiceResult();

            if (id == 0) { result.Success = false; result.Message = "Id can't be null"; return result; };

            try
            {
                _logger.LogInformation($"Getting order header by id: {id}");

                var orderHeader = await _unitOfWork.OrderHeader.GetById(id, "ApplicationUser");

                if (orderHeader == null)
                {
                    result.Message = "Product not found";
                    return result;
                }

                result.Data = orderHeader.GetOrderHeaderModelFromOrderHeader();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Error getting order header by id";
                _logger.LogError($"{result.Message}", ex.ToString());
            }

            return result;
        }

        public async Task<ServiceResult> SaveOrderHeader(OrderHeader orderHeader)
        {
			ServiceResult result = new ServiceResult();

			try
			{
				_logger.LogInformation($"Saving order header");

				await this._unitOfWork.OrderHeader.SaveAsync(orderHeader);
				await this._unitOfWork.Commit();

				result.Message = "Order header successfully saved";
			}
			catch (Exception ex)
			{
				result.Success = false;
				result.Message = "Error saving order header";
				_logger.LogError($"{result.Message}", ex.ToString());
			}

			return result;
		}

        public async Task<ServiceResult> ShipOrder(OrderHeaderModel orderHeaderModel)
        {
            ServiceResult result = new ServiceResult();

            var orderHeader = await _unitOfWork.OrderHeader.GetEntityAsync(orderHeaderModel.Id);

            if (orderHeader == null) { result.Success = false; result.Message = "Id can't be null"; return result; };

            orderHeader.TrackingNumber = orderHeaderModel.TrackingNumber;
            orderHeader.Carrier = orderHeaderModel.Carrier;
            orderHeader.OrderStatus = orderHeaderModel.OrderStatus;
            orderHeader.ShippingDate = orderHeaderModel.ShippingDate;

            if (orderHeader.PaymentStatus == StaticDetails.PaymentStatusDelayedPayment) 
            {
                orderHeader.PaymentDueDate = DateTime.Now.AddDays(30);
            }

            try
            {
                _logger.LogInformation($"Order header set to shipping");

                await this._unitOfWork.OrderHeader.UpdateAsync(orderHeader);
                await this._unitOfWork.Commit();

                result.Message = "Order header successfully set for shipping";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Error setting order header for shipping";
                _logger.LogError($"{result.Message}", ex.ToString());
            }

            return result;
        }

        public async Task<ServiceResult> UpdateOrderHeader(OrderHeaderModel orderHeaderModel)
        {
            ServiceResult result = new ServiceResult();

            var orderHeader = await _unitOfWork.OrderHeader.GetEntityAsync(orderHeaderModel.Id);

            if(orderHeader == null) { result.Success = false; result.Message = "Id can't be null"; return result; };

            orderHeader.Name = orderHeaderModel.Name;
            orderHeader.PhoneNumber = orderHeaderModel.PhoneNumber;
            orderHeader.StreetAddress = orderHeaderModel.StreetAddress;
            orderHeader.City = orderHeaderModel.City;
            orderHeader.State = orderHeaderModel.State;
            orderHeader.PostalCode = orderHeaderModel.PostalCode;

            try
            {
                _logger.LogInformation($"Update order header");

                await this._unitOfWork.OrderHeader.UpdateAsync(orderHeader);
                await this._unitOfWork.Commit();

                result.Message = "Order header successfully saved";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Error saving order header";
                _logger.LogError($"{result.Message}", ex.ToString());
            }

            return result;
        }

        public async Task<ServiceResult> UpdateStatus(int id, string orderStatus, string? paymentStatus = null)
		{
			ServiceResult result = new ServiceResult();

			try
			{
				_logger.LogInformation($"Updating order statis");

				await this._unitOfWork.OrderHeader.UpdateStatus(id, orderStatus, paymentStatus);
				await this._unitOfWork.Commit();

				result.Message = "Order statis successfully update";
			}
			catch (Exception ex)
			{
				result.Success = false;
				result.Message = "Error updating order status";
				_logger.LogError($"{result.Message}", ex.ToString());
			}

			return result;
		}

		public async Task<ServiceResult> UpdateStripePaymentId(int id, string sessionId, string paymentIntentId)
		{
			ServiceResult result = new ServiceResult();

			try
			{
				_logger.LogInformation($"Updating stripe payment id");

				await this._unitOfWork.OrderHeader.UpdateStripePaymentId(id, sessionId, paymentIntentId);
				await this._unitOfWork.Commit();

				result.Message = "Update stripe payment id";
			}
			catch (Exception ex)
			{
				result.Success = false;
				result.Message = "Error updating stripe payment id";
				_logger.LogError($"{result.Message}", ex.ToString());
			}

			return result;
		}
	}
}