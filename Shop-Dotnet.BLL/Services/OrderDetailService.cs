﻿using Microsoft.Extensions.Logging;
using Shop_Dotnet.BLL.Contract;
using Shop_Dotnet.BLL.Core;
using Shop_Dotnet.BLL.Dto.OrderDetail;
using Shop_Dotnet.DAL.Core;
using Shop_Dotnet.DAL.Entities;

namespace Shop_Dotnet.BLL.Services
{
    public class OrderDetailService : IOrderDetailService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger _logger;

        public OrderDetailService(IUnitOfWork unitOfWork, ILogger<OrderDetailService> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        public async Task<ServiceResult> GetAll()
        {
            ServiceResult result = new ServiceResult();

            try
            {
                _logger.LogInformation($"Getting all order details");

                result.Data = await this._unitOfWork.OrderDetail.GetEntitiesAsync("Product");
                result.Message = "Order details successfully displayed";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Error getting order details";
                _logger.LogError($"{result.Message}", ex.ToString());
            }

            return result;
        }

        public async Task<ServiceResult> SaveOrderDetail(OrderDetail orderDetail)
        {
			ServiceResult result = new ServiceResult();

			try
			{
				_logger.LogInformation($"Saving order details");

				await this._unitOfWork.OrderDetail.SaveAsync(orderDetail);
				await this._unitOfWork.Commit();

				result.Message = "Order details successfully saved";
			}
			catch (Exception ex)
			{
				result.Success = false;
				result.Message = "Error saving order details";
				_logger.LogError($"{result.Message}", ex.ToString());
			}

			return result;
		}
    }
}
