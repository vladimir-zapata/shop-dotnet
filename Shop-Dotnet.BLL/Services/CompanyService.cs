﻿using Microsoft.Extensions.Logging;
using Shop_Dotnet.BLL.Contract;
using Shop_Dotnet.BLL.Core;
using Shop_Dotnet.BLL.Dto.Company;
using Shop_Dotnet.BLL.Extensions;
using Shop_Dotnet.DAL.Core;

namespace Shop_Dotnet.BLL.Services
{
    public class CompanyService : ICompanyService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger _logger;

        public CompanyService(IUnitOfWork unitOfWork, ILogger<CompanyService> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        public async Task<ServiceResult> GetAll()
        {
            ServiceResult result = new();

            try
            {
                var companies = await _unitOfWork.Company.GetEntitiesAsync();

                var companiesList = companies
                    .Where(x => !x.Deleted)
                    .Select(x => x.GetCompanyModelFromModel())
                    .ToList();

                result.Data = companiesList;
                result.Message = "Displaying all companies";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Error getting los categories";
                _logger.LogError($"{result.Message}", ex.ToString());
            }

            return result;
        }
        public async Task<ServiceResult> GetById(int id)
        {
            ServiceResult result = new ServiceResult();

            if (id == 0) { result.Success = false; result.Message = "Id can't be null"; return result; };

            try
            {
                _logger.LogInformation($"Getting company by id: {id}");

                var company = await _unitOfWork.Company.GetEntityAsync(id);

                if (company == null)
                {
                    result.Message = "Company not found";
                    return result;
                }

                result.Data = company?.GetCompanyModelFromModel();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Error getting company by id";
                _logger.LogError($"{result.Message}", ex.ToString());
            }

            return result;
        }
        public async Task<ServiceResult> SaveCompany(SaveCompanyDto saveCompanyDto)
        {
            ServiceResult result = new();

            try
            {
                _logger.LogInformation($"Saving company: {saveCompanyDto.Name}");

                var company = saveCompanyDto.GetCompanyFromSaveCompanyDto();
                company.CreationUser = saveCompanyDto.RequestUser;

                await this._unitOfWork.Company.SaveAsync(company);
                await this._unitOfWork.Commit();

                result.Message = "Company successfully saved";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Error saving company";
                _logger.LogError($"{result.Message}", ex.ToString());
            }

            return result;
        }
        public async Task<ServiceResult> UpdateCompany(UpdateCompanyDto updateCompanyDto)
        {
            ServiceResult result = new ServiceResult();

            if (updateCompanyDto.Id == null || updateCompanyDto.Id == 0) 
            {
                result.Success = false;
                result.Message = "Id can't be null";
                return result;
            }

            try
            {
                _logger.LogInformation($"Updating company id: {updateCompanyDto.Id}");

                var company = await this._unitOfWork.Company.GetEntityAsync(updateCompanyDto.Id!.Value);

                if (company == null)
                {
                    result.Success = true;
                    result.Message = "Company not found";
                    return result;
                }

                company.Name = updateCompanyDto.Name;
                company.StreetAddress = updateCompanyDto.StreetAddress;
                company.City = updateCompanyDto.City;
                company.State = updateCompanyDto.State;
                company.PostalCode= updateCompanyDto.PostalCode;
                company.PhoneNumber = updateCompanyDto.PhoneNumber;
                company.ModifyDate = DateTime.Now;
                company.ModifyUser = updateCompanyDto.RequestUser;

                await this._unitOfWork.Company.UpdateAsync(company);
                await this._unitOfWork.Commit();

                result.Message = "Company successfully updated";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Error updating company";
                _logger.LogError($"{result.Message}", ex.ToString());
            }

            return result;
        }
        public async Task<ServiceResult> DeleteCompany(DeleteCompanyDto deleteCompanyDto)
        {
            ServiceResult result = new ServiceResult();

            if (deleteCompanyDto.Id == null || deleteCompanyDto.Id == 0)
            {
                result.Success = false;
                result.Message = "Id can't be null";
                return result;
            }

            try
            {
                _logger.LogInformation($"Deleting company id: {deleteCompanyDto.Id}");

                var company = await this._unitOfWork.Company.GetEntityAsync(deleteCompanyDto.Id!.Value);

                if (company == null)
                {
                    result.Success = true;
                    result.Message = "Company not found";
                    return result;
                }

                company.Deleted = true;
                company.DeleteUser = deleteCompanyDto.RequestUser;
                company.DeleteDate = DateTime.Now;

                await this._unitOfWork.Company.UpdateAsync(company);
                await this._unitOfWork.Commit();

                result.Message = "Company successfully deleted";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Error deleting company";
                _logger.LogError($"{result.Message}", ex.ToString());
            }

            return result;
        }
    }
}
