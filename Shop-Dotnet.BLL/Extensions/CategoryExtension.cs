﻿using Shop_Dotnet.BLL.Dto.Category;
using Shop_Dotnet.BLL.Models;
using Shop_Dotnet.DAL.Entities;

namespace Shop_Dotnet.BLL.Extensions
{
    public static class CategoryExtension
    {
        public static CategoryModel GetCategoryModelFromCategory(this Category category)
        {
            return new()
            {
                Id = (int) category.Id!,
                Name = category.Name,
                DisplayOrder = category.DisplayOrder,
            };
        }

        public static Category GetCategoryFromSaveCategoryDto(this SaveCategoryDto saveCategoryDto)
        {
            return new()
            {
                Name = saveCategoryDto.Name,
                DisplayOrder = saveCategoryDto.DisplayOrder,
                CreationUser = saveCategoryDto.RequestUser,
                CreationDate = DateTime.Now
            };
        }

        public static Category GetCategoryFromUpdateCategoryDto(this UpdateCategoryDto updateCategoryDto)
        {
            return new()
            {
                Id = updateCategoryDto.Id,
                Name = updateCategoryDto.Name,
                DisplayOrder = updateCategoryDto.DisplayOrder,
                ModifyUser = updateCategoryDto.RequestUser,
                ModifyDate = DateTime.Now
            };
        }
    }
}
