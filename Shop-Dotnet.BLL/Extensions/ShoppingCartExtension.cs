﻿using Shop_Dotnet.BLL.Dto.Category;
using Shop_Dotnet.BLL.Dto.ShoppingCart;
using Shop_Dotnet.BLL.Models;
using Shop_Dotnet.DAL.Entities;

namespace Shop_Dotnet.BLL.Extensions
{
    public static class ShoppingCartExtension
    {
        public static ShoppingCartModel GetShoppingCartModelFromShoppingCart(this ShoppingCart shoppingCart) 
        {
            return new()
            {
                Id = shoppingCart.Id,
                ProductId = shoppingCart.ProductId,
                Product = shoppingCart.Product,
                Count = shoppingCart.Count,
                ApplicationUserId = shoppingCart.ApplicationUserId
            };
        }

		public static ShoppingCart GetShoppingCartFromShoppingCartModel(this ShoppingCartModel shoppingCart)
		{
			return new()
			{
				Id = shoppingCart.Id,
				ProductId = shoppingCart.ProductId!.Value,
				Product = shoppingCart.Product,
				Count = shoppingCart.Count,
				ApplicationUserId = shoppingCart.ApplicationUserId
			};
		}

		public static ShoppingCart GetShoppingCartFromSaveShoppingCartDto(this SaveShoppingCartDto saveShoppingCartDto)
        {
            return new()
            {
                ProductId = saveShoppingCartDto.ProductId,
                Count = saveShoppingCartDto.Count,
                ApplicationUserId = saveShoppingCartDto.ApplicationUserId,
                CreationUser = saveShoppingCartDto.RequestUser,
                CreationDate = DateTime.Now
            };
        }

        public static SaveShoppingCartDto GetSaveShoppingCartDtoFromShoppingCart(this ShoppingCart shoppingCart)
        {
            return new()
            {
                ProductId = shoppingCart.ProductId,
                Count = shoppingCart.Count,
                ApplicationUserId = shoppingCart.ApplicationUserId,
                RequestUser = shoppingCart.ApplicationUserId
            };
        }

        public static UpdateShoppingCartDto GetUpdateShoppingCartDtoFromShoppingCartModel(this ShoppingCartModel shoppingCartModel)
        {
            return new()
            {
                Id = shoppingCartModel.Id,
                ProductId = shoppingCartModel.ProductId!.Value,
                Count = shoppingCartModel.Count,
                ApplicationUserId = shoppingCartModel.ApplicationUserId,
                RequestUser = shoppingCartModel.ApplicationUserId
            };
        }

        public static DeleteShoppingCartDto GetDeleteShoppingCartDtoFromShoppingCartModel(this ShoppingCartModel shoppingCartModel)
        {
            return new()
            {
                Id = shoppingCartModel.Id,
                RequestUser = shoppingCartModel.ApplicationUserId
            };
        }
    }
}
