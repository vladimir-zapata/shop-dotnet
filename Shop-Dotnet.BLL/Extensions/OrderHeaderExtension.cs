﻿using Shop_Dotnet.BLL.Models;
using Shop_Dotnet.DAL.Entities;

namespace Shop_Dotnet.BLL.Extensions
{
    public static class OrderHeaderExtension
    {
        public static OrderHeaderModel GetOrderHeaderModelFromOrderHeader(this OrderHeader orderHeader)
        {
            return new OrderHeaderModel()
            {
                Id = orderHeader.Id!.Value,
                ApplicationUserId = orderHeader.ApplicationUserId,
                ApplicationUser = orderHeader.ApplicationUser,
                OrderDate = orderHeader.OrderDate,
                ShippingDate = orderHeader.ShippingDate,
                OrderTotal = orderHeader.OrderTotal,
                OrderStatus = orderHeader.OrderStatus,
                PaymentStatus = orderHeader.PaymentStatus,
                TrackingNumber = orderHeader.TrackingNumber,
                Carrier = orderHeader.Carrier,
                PaymentDate = orderHeader.PaymentDate,
                PaymentDueDate = orderHeader.PaymentDueDate,
                SessionId = orderHeader.SessionId,
                PaymentIntentId = orderHeader.PaymentIntentId,
                PhoneNumber = orderHeader.PhoneNumber,
                StreetAddress = orderHeader.StreetAddress,
                City = orderHeader.City,
                State = orderHeader.State,
                PostalCode = orderHeader.PostalCode,
                Name = orderHeader.Name
            };
        }

        public static OrderHeader GetOrderHeaderFromOrderHeaderModel(this OrderHeaderModel orderHeader)
        {
            return new OrderHeader()
            {
                Id = orderHeader.Id,
                ApplicationUserId = orderHeader.ApplicationUserId,
                ApplicationUser = orderHeader.ApplicationUser,
                OrderDate = orderHeader.OrderDate,
                ShippingDate = orderHeader.ShippingDate,
                OrderTotal = orderHeader.OrderTotal,
                OrderStatus = orderHeader.OrderStatus,
                PaymentStatus = orderHeader.PaymentStatus,
                TrackingNumber = orderHeader.TrackingNumber,
                Carrier = orderHeader.Carrier,
                PaymentDate = orderHeader.PaymentDate,
                PaymentDueDate = orderHeader.PaymentDueDate,
                SessionId = orderHeader.SessionId,
                PaymentIntentId = orderHeader.PaymentIntentId,
                PhoneNumber = orderHeader.PhoneNumber,
                StreetAddress = orderHeader.StreetAddress,
                City = orderHeader.City,
                State = orderHeader.State,
                PostalCode = orderHeader.PostalCode,
                Name = orderHeader.Name
            };
        }
    }
}
