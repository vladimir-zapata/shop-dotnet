﻿using Shop_Dotnet.BLL.Dto.Category;
using Shop_Dotnet.BLL.Dto.Product;
using Shop_Dotnet.BLL.Models;
using Shop_Dotnet.DAL.Entities;

namespace Shop_Dotnet.BLL.Extensions
{
    public static class ProductExtension
    {
        public static ProductModel GetProductModelFromProduct(this Product product)
        {
            return new()
            {
                Id = (int)product.Id!,
                Title = product.Title,
                Author = product.Author,
                Description = product.Description,
                ISBN = product.ISBN,
                ListPrice = product.ListPrice,
                Price = product.Price,
                Price50 = product.Price50,
                Price100 = product.Price100,
                CategoryId = product.CategoryId,
                ImageUrl = product.ImageUrl,
                Category = product.Category == null ? new CategoryModel() : new CategoryModel()
                {
                    Name = product.Category.Name,
                },
            };
        }
        public static Product GetProductFromSaveProductDto(this SaveProductDto saveProductDto)
        {
            return new()
            {
                Title = saveProductDto.Title,
                Author = saveProductDto.Author,
                Description = saveProductDto.Description,
                ISBN = saveProductDto.ISBN,
                ListPrice = saveProductDto.ListPrice,
                Price = saveProductDto.Price,
                Price50 = saveProductDto.Price50,
                Price100 = saveProductDto.Price100,
                CategoryId = saveProductDto.CategoryId,
                ImageUrl = saveProductDto.ImageUrl,
                CreationUser = saveProductDto.RequestUser,
                CreationDate = DateTime.Now,
            };
        }
        public static Product GetProductFromUpdateProductDto(this UpdateProductDto updateProductDto)
        {
            return new()
            {
                Id = updateProductDto.Id,
                Title = updateProductDto.Title,
                Author = updateProductDto.Author,
                Description = updateProductDto.Description,
                ISBN = updateProductDto.ISBN,
                ListPrice = updateProductDto.ListPrice,
                Price = updateProductDto.Price,
                Price50 = updateProductDto.Price50,
                Price100 = updateProductDto.Price100,
                ImageUrl = updateProductDto.ImageUrl,
                CategoryId = updateProductDto.CategoryId,
                CreationUser = updateProductDto.RequestUser,
                CreationDate = DateTime.Now
            };
        }

        public static SaveProductDto GetSaveProductDtoFromProduct(this Product product)
        {
            return new()
            {
                Title = product.Title,
                Author = product.Author,
                Description = product.Description,
                ISBN = product.ISBN,
                ListPrice = product.ListPrice,
                Price = product.Price,
                Price50 = product.Price50,
                Price100 = product.Price100,
                ImageUrl = product.ImageUrl,
                CategoryId = product.CategoryId,
            };
        }

        public static SaveProductDto GetSaveProductDtoFromProductModel(this ProductModel product)
        {
            return new()
            {
                Title = product.Title,
                Author = product.Author,
                Description = product.Description,
                ISBN = product.ISBN,
                ListPrice = product.ListPrice,
                Price = product.Price,
                Price50 = product.Price50,
                Price100 = product.Price100,
                ImageUrl = product.ImageUrl,
                CategoryId = product.CategoryId,
            };
        }

        public static UpdateProductDto GetUpdateProductDtoFromProduct(this Product product)
        {
            return new()
            {
                Id = product.Id,
                Title = product.Title,
                Author = product.Author,
                Description = product.Description,
                ISBN = product.ISBN,
                ListPrice = product.ListPrice,
                Price = product.Price,
                Price50 = product.Price50,
                Price100 = product.Price100,
                ImageUrl = product.ImageUrl,
                CategoryId = product.CategoryId,
            };
        }

        public static UpdateProductDto GetUpdateProductDtoFromProductModel(this ProductModel product)
        {
            return new()
            {
                Title = product.Title,
                Author = product.Author,
                Description = product.Description,
                ISBN = product.ISBN,
                ListPrice = product.ListPrice,
                Price = product.Price,
                Price50 = product.Price50,
                Price100 = product.Price100,
                ImageUrl = product.ImageUrl,
                CategoryId = product.CategoryId,
            };
        }
    }
}
