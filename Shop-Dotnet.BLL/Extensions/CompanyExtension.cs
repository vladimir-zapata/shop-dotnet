﻿using Shop_Dotnet.BLL.Dto.Company;
using Shop_Dotnet.BLL.Models;
using Shop_Dotnet.DAL.Entities;

namespace Shop_Dotnet.BLL.Extensions
{
    public static class CompanyExtension
    {
        public static CompanyModel GetCompanyModelFromModel(this Company company)
        {
            return new()
            {
                Id = company.Id,
                Name = company.Name,
                StreetAddress = company.StreetAddress,
                City = company.City,
                State= company.State,   
                PostalCode = company.PostalCode,
                PhoneNumber = company.PhoneNumber,
            };
        }

        public static Company GetCompanyFromSaveCompanyDto(this SaveCompanyDto saveCompanyDto)
        {
            return new()
            {
                Name = saveCompanyDto.Name,
                StreetAddress = saveCompanyDto.StreetAddress,
                City = saveCompanyDto.City,
                State = saveCompanyDto.State,
                PostalCode = saveCompanyDto.PostalCode,
                PhoneNumber = saveCompanyDto.PhoneNumber,
                CreationUser = Guid.NewGuid().ToString(),
                CreationDate = DateTime.Now,
            };
        }

        public static Company GetCompanyFromUpdateCompanyDto(this UpdateCompanyDto updateCompanyDto)
        {
            return new()
            {
                Id = updateCompanyDto.Id,
                Name = updateCompanyDto.Name,
                StreetAddress = updateCompanyDto.StreetAddress,
                City = updateCompanyDto.City,
                State = updateCompanyDto.State,
                PostalCode = updateCompanyDto.PostalCode,
                PhoneNumber = updateCompanyDto.PhoneNumber,
                CreationUser = Guid.NewGuid().ToString(),
                CreationDate = DateTime.Now,
            };
        }

        public static SaveCompanyDto GetSaveCompanyDtoFromCompany(this Company company)
        {
            return new()
            {
                Name = company.Name,
                StreetAddress = company.StreetAddress,
                City = company.City,
                State = company.State,
                PostalCode = company.PostalCode,
                PhoneNumber = company.PhoneNumber,
            };
        }

        public static SaveCompanyDto GetSaveCompanyDtoFromCompanyModel(this CompanyModel company)
        {
            return new()
            {
                Name = company.Name,
                StreetAddress = company.StreetAddress,
                City = company.City,
                State = company.State,
                PostalCode = company.PostalCode,
                PhoneNumber = company.PhoneNumber,
            };
        }

        public static UpdateCompanyDto GetUpdateCompanyDtoFromCompany(this Company company)
        {
            return new()
            {
                Id = company.Id,
                Name = company.Name,
                StreetAddress = company.StreetAddress,
                City = company.City,
                State = company.State,
                PostalCode = company.PostalCode,
                PhoneNumber = company.PhoneNumber,
            };
        }

        public static UpdateCompanyDto GetUpdateCompanyDtoFromCompanyModel(this CompanyModel company)
        {
            return new()
            {
                Id = company.Id,
                Name = company.Name,
                StreetAddress = company.StreetAddress,
                City = company.City,
                State = company.State,
                PostalCode = company.PostalCode,
                PhoneNumber = company.PhoneNumber,
            };
        }
    }
}