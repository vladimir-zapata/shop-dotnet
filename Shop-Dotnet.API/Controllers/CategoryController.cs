﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shop_Dotnet.BLL.Contract;
using Shop_Dotnet.BLL.Dto.Category;

namespace Shop_Dotnet.API.Controllers
{
    [Route("api/v1/categories")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            this._categoryService = categoryService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            return Ok( await this._categoryService.GetAll());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetCategoryById(int id)
        {
            return Ok(await this._categoryService.GetById(id));
        }

        [HttpPost]
        public async Task<IActionResult> SaveCategory(SaveCategoryDto saveCategoryDto)
        {
            return Ok(await this._categoryService.SaveCategory(saveCategoryDto));
        }

        [HttpPost("update")]
        public async Task<IActionResult> UpdateCategory(UpdateCategoryDto updateCategoryDto)
        {
            return Ok(await this._categoryService.UpdateCategory(updateCategoryDto));
        }
        [HttpPost("delete")]
        public async Task<IActionResult> DeleteCategory(DeleteCategoryDto deleteCategoryDto)
        {
            return Ok(await this._categoryService.DeleteCategory(deleteCategoryDto));
        }
    }
}
