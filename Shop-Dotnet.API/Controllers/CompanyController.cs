﻿using Microsoft.AspNetCore.Mvc;
using Shop_Dotnet.BLL.Contract;
using Shop_Dotnet.BLL.Dto.Company;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Shop_Dotnet.API.Controllers
{
    [Route("api/v1/companies")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        private readonly ICompanyService _companyService;

        public CompanyController(ICompanyService companyService)
        {
            _companyService = companyService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await _companyService.GetAll());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetCompany(int id)
        {
            return Ok(await _companyService.GetById(id));
        }

        [HttpPost]
        public async Task<IActionResult> CreateCompany(SaveCompanyDto saveCompanyDto)
        {
            return Ok(await _companyService.SaveCompany(saveCompanyDto));
        }

        [HttpPost("update")]
        public async Task<IActionResult> UpdateCompany(UpdateCompanyDto updateCompanyDto) 
        {
            return Ok(await _companyService.UpdateCompany(updateCompanyDto));
        }

        [HttpPost("delete")]
        public async Task<IActionResult> DeleteCompany(DeleteCompanyDto deleteCompanyDto)
        {
            return Ok(await _companyService.DeleteCompany(deleteCompanyDto));
        }
    }
}
