﻿using Microsoft.AspNetCore.Mvc;
using Shop_Dotnet.BLL.Contract;

namespace Shop_Dotnet.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderHeaderController : ControllerBase
    {
        private readonly IOrderHeaderService _orderHeaderService;

        public OrderHeaderController(IOrderHeaderService orderHeaderService)
        {
            _orderHeaderService = orderHeaderService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _orderHeaderService.GetAll());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await _orderHeaderService.GetById(id));
        }
    }
}
