﻿using Microsoft.AspNetCore.Mvc;
using Shop_Dotnet.BLL.Contract;
using Shop_Dotnet.BLL.Dto.ShoppingCart;

namespace Shop_Dotnet.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShoppingCartController : ControllerBase
    {
        private readonly IShoppingCartService _shoppingCartService;

        public ShoppingCartController(IShoppingCartService shoppingCartService)
        {
            _shoppingCartService = shoppingCartService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await _shoppingCartService.GetAll());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await _shoppingCartService.GetById(id));
        }

        [HttpPost]
        public async Task<IActionResult> SaveShoppingCart(SaveShoppingCartDto saveShoppingCartDto)
        {
            return Ok(await _shoppingCartService.SaveShoppingCart(saveShoppingCartDto));
        }

        [HttpPost("update")]
        public async Task<IActionResult> UpdateShoppingCart(UpdateShoppingCartDto updateShoppingCartDto)
        {
            return Ok(await _shoppingCartService.UpdateShoppingCart(updateShoppingCartDto));
        }

        [HttpPost("delete")]
        public async Task<IActionResult> DeleteShoppingCart(DeleteShoppingCartDto deleteShoppingCartDto)
        {
            return Ok(await _shoppingCartService.DeleteShoppingCart(deleteShoppingCartDto));
        }
    }
}
