﻿using Microsoft.AspNetCore.Mvc;
using Shop_Dotnet.BLL.Contract;
using Shop_Dotnet.BLL.Dto.Product;

namespace Shop_Dotnet.API.Controllers
{
    [Route("api/v1/products")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            this._productService = productService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllProducts()
        {
            return Ok(await _productService.GetAll());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetProductById(int id)
        {
            return Ok(await _productService.GetById(id));
        }

        [HttpPost]
        public async Task<IActionResult> SaveProduct(SaveProductDto saveProductDto)
        {
            return Ok(await _productService.SaveProduct(saveProductDto));
        }

        [HttpPost("update")]
        public async Task<IActionResult> UpdateProduct(UpdateProductDto updateProductDto)
        {
            return Ok(await _productService.UpdateProduct(updateProductDto));
        }

        [HttpPost("delete")]
        public async Task<IActionResult> UpdateProduct(DeleteProductDto deleteProductDto)
        {
            return Ok(await _productService.DeleteProduct(deleteProductDto));
        }
    }
}
