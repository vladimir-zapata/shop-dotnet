﻿using Shop_Dotnet.BLL.Contract;
using Shop_Dotnet.BLL.Services;
using Shop_Dotnet.DAL.Interfaces;
using Shop_Dotnet.DAL.Repositories;

namespace Shop_Dotnet.API.Dependencies
{
    public static class ProductDependencies
    {
        public static void AddProductDependencies(this WebApplicationBuilder builder)
        {
            builder.Services.AddScoped<IProductRepository, ProductRepository>();
            builder.Services.AddScoped<IProductService, ProductService>();
        }
    }
}
