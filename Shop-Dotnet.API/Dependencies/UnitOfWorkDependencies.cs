﻿using Shop_Dotnet.DAL.Core;

namespace Shop_Dotnet.API.Dependencies
{
    public static class UnitOfWorkDependencies
    {
        public static void AddUnitOfWorkDependencies(this WebApplicationBuilder builder) 
        {
            builder.AddCategoryDependencies();
            builder.AddProductDependencies();
            builder.AddCompanyDependencies();
            builder.AddShoppingCartDependencies();
            builder.AddOrderHeaderDependencies();
            builder.AddOrderDetailDependencies();
            builder.AddApplicationUserDependencies();

            builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
        }
    }
}
