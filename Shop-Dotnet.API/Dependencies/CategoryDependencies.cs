﻿using Shop_Dotnet.BLL.Contract;
using Shop_Dotnet.BLL.Services;
using Shop_Dotnet.DAL.Interfaces;
using Shop_Dotnet.DAL.Repositories;

namespace Shop_Dotnet.API.Dependencies
{
    public static class CategoryDependencies
    {
        public static void AddCategoryDependencies(this WebApplicationBuilder builder)
        {
            builder.Services.AddScoped<ICategoryRepository, CategoryRepository>();
            builder.Services.AddScoped<ICategoryService, CategoryService>();
        }
    }
}
