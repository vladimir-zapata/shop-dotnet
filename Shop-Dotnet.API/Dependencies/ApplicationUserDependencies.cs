﻿using Shop_Dotnet.BLL.Contract;
using Shop_Dotnet.BLL.Services;
using Shop_Dotnet.DAL.Interfaces;
using Shop_Dotnet.DAL.Repositories;

namespace Shop_Dotnet.API.Dependencies
{
    public static class ApplicationUserDependencies
    {
        public static void AddApplicationUserDependencies(this WebApplicationBuilder builder) 
        {
            builder.Services.AddScoped<IApplicationUserRepository, ApplicationUserRepository>();
            builder.Services.AddScoped<IApplicationUserService, ApplicationUserService>();
        }
    }
}
