﻿using Shop_Dotnet.BLL.Contract;
using Shop_Dotnet.BLL.Services;
using Shop_Dotnet.DAL.Interfaces;
using Shop_Dotnet.DAL.Repositories;

namespace Shop_Dotnet.API.Dependencies
{
    public static class OrderHeaderDependencies
    {
        public static void AddOrderHeaderDependencies(this WebApplicationBuilder builder)
        {
            builder.Services.AddScoped<IOrderHeaderRepository, OrderHeaderRepository>();
            builder.Services.AddScoped<IOrderHeaderService, OrderHeaderService>();
        }
    }
}
